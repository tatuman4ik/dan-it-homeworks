import React, {useEffect, useState} from 'react';
import axios from 'axios';

const UserList = () => {
    const [users, setUsers] = useState([]);
    const [selectedUser, setSelectedUser] = useState(null);
    const [isEditing, setIsEditing] = useState(false);
    const [isNewUser, setIsNewUser] = useState(false);
    const [editedName, setEditedName] = useState('');
    const [editedEmail, setEditedEmail] = useState('');
    const [editedAge, setEditedAge] = useState('');
    const [isNewAccount, setIsNewAccount] = useState(false);
    const [newAccountCurrency, setNewAccountCurrency] = useState('');


    const fetchUsers = async () => {
        try {
            const response = await axios.get('http://localhost:9000/api/customers');
            setUsers(response.data);
        } catch (error) {
            console.error('Error fetching users:', error);
        }
    };

    useEffect(() => {
        fetchUsers();
    }, []);

    const handleUserSelect = (user) => {
        setSelectedUser(user);
        setEditedName(user.name);
        setEditedEmail(user.email);
        setEditedAge(user.age);
        setIsEditing(true);
    };

    const handleUpdateUser = async (userId) => {
        const updatedData = {
            name: editedName,
            email: editedEmail,
            age: editedAge
        };

        try {
            await axios.put(`http://localhost:9000/api/customers/${userId}`, updatedData);
            fetchUsers();
            setSelectedUser(null);
            setIsEditing(false);
            setIsNewUser(false);
        } catch (error) {
            console.error('Error updating user:', error);
        }
    };

    const handleCreateUser = async () => {
        const newUser = {
            name: editedName,
            email: editedEmail,
            age: editedAge
        };

        try {
            await axios.post('http://localhost:9000/api/customers', newUser);
            fetchUsers();
            setEditedName('');
            setEditedEmail('');
            setEditedAge('');
            setIsEditing(false);
            setIsNewUser(false);
        } catch (error) {
            console.error('Error creating user:', error);
        }
    };

    const handleDeleteUser = async (userId) => {
        try {
            await axios.delete(`http://localhost:9000/api/customers/${userId}`);
            fetchUsers();
            setSelectedUser(null);
            setIsNewUser(false);
        } catch (error) {
            console.error('Error deleting user:', error);
        }
    };

    const handleCreateAccount = async (customerId) => {
        const newAccountData = {
            currency: newAccountCurrency
        };

        try {
            await axios.post(`http://localhost:9000/api/customers/${customerId}/accounts`, newAccountData);
            fetchUsers();
            setNewAccountCurrency('');
            setIsNewAccount(false);
        } catch (error) {
            console.error('Error creating account:', error);
        }
    };

    return (
        <div>
            <h1>User List</h1>
            <button onClick={() => setIsNewUser(true)}>Create New User</button>
            <ul>
                {users.map(user => (
                    <li key={user.id}>
                        <strong> ID:</strong> {user.id}
                        <strong> Name:</strong> {user.name} <br/>
                        <button onClick={() => handleUserSelect(user)}>Edit</button>
                        <button onClick={() => handleDeleteUser(user.id)}>Delete</button>
                        <br/>
                        <strong>Accounts:</strong>
                        <ul>
                            {user.accounts.map(account => (
                                <li key={account.id}>
                                    <strong> Account ID:</strong> {account.id}
                                    <strong> Currency:</strong> {account.currency}
                                    <strong> Balance:</strong> {account.balance}
                                </li>
                            ))}
                            <li>
                                {isNewAccount ? (
                                    <div>
                                        <label>Currency: </label>
                                        <select value={newAccountCurrency}
                                                onChange={(e) => setNewAccountCurrency(e.target.value)}>
                                            <option value="">Select Currency</option>
                                            <option value="USD">USD</option>
                                            <option value="EUR">EUR</option>
                                            <option value="UAH">UAH</option>
                                            <option value="CHF">CHF</option>
                                            <option value="GBP">GBP</option>
                                        </select>
                                        <button onClick={() => handleCreateAccount(user.id)}>Create Account</button>
                                        <button onClick={() => setIsNewAccount(false)}>Cancel</button>
                                    </div>
                                ) : (
                                    <button onClick={() => setIsNewAccount(true)}>Create New Account</button>
                                )}
                            </li>
                        </ul>
                    </li>
                ))}
            </ul>
            {(isEditing || isNewUser) && (
                <div>
                    <h2>{isNewUser ? 'Create User' : 'Edit User'}</h2>
                    <form>
                        <label>Name: </label>
                        <input type="text" value={editedName} onChange={(e) => setEditedName(e.target.value)}/>
                        <label>Email: </label>
                        <input type="text" value={editedEmail} onChange={(e) => setEditedEmail(e.target.value)}/>
                        <label>Age: </label>
                        <input type="text" value={editedAge} onChange={(e) => setEditedAge(e.target.value)}/>
                        <button onClick={() => isNewUser ? handleCreateUser() : handleUpdateUser(selectedUser.id)}>
                            {isNewUser ? 'Create' : 'Save'}
                        </button>
                        <button onClick={() => {
                            setIsEditing(false);
                            setIsNewUser(false);
                        }}>
                            Cancel
                        </button>
                    </form>
                </div>
            )}
            {isNewAccount && (
                <div>
                    <h2>Create New Account</h2>
                    <form>
                        {/* Add input fields for account details */}
                        {/* Add a Save button to create the new account */}
                        <button onClick={() => setIsNewAccount(false)}>Cancel</button>
                    </form>
                </div>
            )}

        </div>
    );
};

export default UserList;

