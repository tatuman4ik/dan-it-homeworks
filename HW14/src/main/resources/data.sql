CREATE TABLE Customers
(
    id         BIGINT AUTO_INCREMENT PRIMARY KEY,
    cust_name  VARCHAR(255),
    cust_email VARCHAR(255),
    cust_age   INT
);

CREATE TABLE Accounts
(
    id          BIGINT AUTO_INCREMENT PRIMARY KEY,
    ac_uuid     VARCHAR(255),
    currency    INT4,
    balance     DOUBLE,
    customer_id BIGINT,
    FOREIGN KEY (customer_id) REFERENCES Customers (id)
);

CREATE TABLE Employers
(
    id        BIGINT AUTO_INCREMENT PRIMARY KEY,
    empl_name VARCHAR(255),
    empl_addr VARCHAR(255)
);

CREATE TABLE Jobs
(
    id          BIGINT AUTO_INCREMENT PRIMARY KEY,
    customer_id BIGINT,
    employer_id BIGINT,
    FOREIGN KEY (customer_id) REFERENCES Customers (id),
    FOREIGN KEY (employer_id) REFERENCES Employers (id)
);

INSERT INTO Customers (cust_name, cust_email, cust_age)
VALUES ('Taras', 'taras@example.com', 30),
       ('Petro', 'petro@example.com', 25),
       ('Mykola', 'mykola@example.com', 28);

INSERT INTO Accounts (ac_uuid, currency, balance, customer_id)
VALUES ('ac123', 1, 1000.0, 1),
       ('ac456', 2, 800.0, 1),
       ('ac789', 3, 1500.0, 2);

INSERT INTO Employers (empl_name, empl_addr)
VALUES ('ABC Corporation', 'Peremohy, 12, 3'),
       ('XYZ Industries', 'Vydradnyi, 4-A'),
       ('123 Enterprises', 'Lukjanivska, 7/89');

INSERT INTO Jobs (customer_id, employer_id)
VALUES (1, 1),
       (1, 2),
       (2, 2),
       (3, 3);
