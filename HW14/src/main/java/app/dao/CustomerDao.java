package app.dao;

import app.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDao extends GenericDao<Customer> {
    public CustomerDao(JpaRepository<Customer, Long> repository) {
        super(repository);
    }
}
