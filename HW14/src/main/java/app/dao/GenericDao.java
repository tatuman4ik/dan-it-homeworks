package app.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@RequiredArgsConstructor
public abstract class GenericDao<T> implements Dao<T> {

    private final JpaRepository<T, Long> repository;

    @Override
    public T save(T entity) {
        return repository.save(entity);
    }

    @Override
    public void saveAll(List<T> entities) {
        repository.saveAll(entities);
    }

    @Override
    public boolean deleteById(long id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(T entity) {
        if (repository.findAll().contains(entity)) {
            repository.delete(entity);
            return true;
        }
        return false;
    }

    @Override
    public void deleteAll(List<T> entities) {
        repository.deleteAll(entities);
    }

    @Override
    public List<T> findAll() {
        return repository.findAll();
    }

    @Override
    public T getOne(long id) {
        return repository.findById(id).orElse(null);
    }
}
