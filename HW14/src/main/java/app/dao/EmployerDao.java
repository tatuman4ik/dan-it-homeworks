package app.dao;

import app.models.Employer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public class EmployerDao extends GenericDao<Employer> {
    public EmployerDao(JpaRepository<Employer, Long> repository) {
        super(repository);
    }
}
