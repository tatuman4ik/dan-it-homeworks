package app.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Employers")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Employer extends AbstractEntity {

    @Column(name = "empl_name")
    private String company;

    @Column(name = "empl_addr")
    private String address;

    @OneToMany(mappedBy = "employer", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JsonManagedReference
    @JsonIgnore
    private List<Job> jobs;

}
