package app.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Customers")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Customer extends AbstractEntity {

    @Column(name = "cust_name")
    private String name;

    @Column(name = "cust_email")
    private String email;

    @Column(name = "cust_age")
    private Integer age;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JsonManagedReference
    @JsonIgnore
    private List<Account> accounts;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JsonManagedReference
    @JsonIgnore
    private List<Job> jobs;

    public Customer(@NonNull String name, @NonNull String email, @NonNull Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }
}
