package app.controller;

import app.models.Job;
import app.service.JobService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/jobs")
@AllArgsConstructor
public class JobController {

    private final JobService jobService;
    private final CustomerController customerController;
    private final EmployerController employerController;

    @PostMapping("/{customerId}/{employerId}")
    public Job createJob(@PathVariable long customerId, @PathVariable long employerId) {
        return jobService.create(customerController.getCustomer(customerId),
                employerController.getEmployer(employerId));
    }

    @DeleteMapping("/{jobId}")
    public void deleteJob(@PathVariable Long jobId) {
        jobService.delete(jobId);
    }
}
