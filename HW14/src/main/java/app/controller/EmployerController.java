package app.controller;

import app.models.Employer;
import app.service.EmployerService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/employers")
public class EmployerController {

    private final EmployerService employerService;

    public EmployerController(EmployerService employerService) {
        this.employerService = employerService;
    }

    @GetMapping("/{employerId}")
    public Employer getEmployer(@PathVariable long employerId) {
        return employerService.getOneWithId(employerId);
    }

    @PostMapping()
    public Employer createEmployer(@RequestBody Employer employer) {
        return employerService.create(employer);
    }

    @PutMapping("/{employerId}")
    public Employer updateEmployer(@PathVariable Long employerId, @RequestBody Employer employer) {
        return employerService.update(employerId, employer.getCompany(), employer.getAddress());
    }

    @DeleteMapping("/{employerId}")
    public void deleteEmployer(@PathVariable Long employerId) {
        employerService.deleteEmployer(employerId);
    }
}
