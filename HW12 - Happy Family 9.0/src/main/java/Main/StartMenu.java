package Main;

import Human.Human;
import Human.Man;
import Human.Woman;

import java.lang.reflect.Constructor;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;

public class StartMenu {
    FamilyController familyController;
    boolean quit;

    Map<Integer, String> StartMap = new HashMap<>() {{
        put(1, "Generate 10 families");
        put(2, "Show list of all families");
        put(3, "Families with more than X persons");
        put(4, "Families with less than X persons");
        put(5, "Families with X persons");
        put(6, "Create new family by parameters");
        put(7, "Delete family by index");
        put(8, "Add child to family by index");
        put(9, "Delete all children older than X years");
    }};

    public StartMenu(FamilyController familyController) {
        this.familyController = familyController;
        while (!quit) {
            Screen.print("Choose what to do next. \"Exit\" for quit");
            for (Map.Entry<Integer, String> entry : StartMap.entrySet()) {
                Screen.print(entry.getKey() + ": " + entry.getValue());
            }
            String userChoice = Screen.read().toLowerCase();
            if (userChoice.equals("exit")) quit = true;
            if (userChoice.matches("^[1-9]$") && StartMap.containsKey(Integer.parseInt(userChoice)))
                doSelection(Integer.parseInt(userChoice));
        }
    }

    public void doSelection(int selection) {
        if (isEmpty()) {
            switch (selection) {
                case 1 -> {
                    RandomGenerator rg = new RandomGenerator();
                    rg.generateTestFamilies(familyController);
                }
                case 6 -> Screen.print(familyController.createNewFamily(createHuman("mother", Woman.class), createHuman("father", Man.class)).prettyFormat());
                default -> Screen.print("Nothing to show yet");
            }
        } else {
            switch (selection) {
                case 1 -> {
                    RandomGenerator rg = new RandomGenerator();
                    rg.generateTestFamilies(familyController);
                }
                case 2 -> familyController.displayAllFamilies();
                case 3 -> familyController.getFamiliesBiggerThan(checkNumber("number", 0, 20));
                case 4 -> familyController.getFamiliesLessThan(checkNumber("number", 0, 20));
                case 5 -> Screen.print(Long.toString(familyController.countFamiliesWithMemberNumber(checkNumber("number", 0, 20))));
                case 6 -> Screen.print(familyController.createNewFamily(createHuman("mother", Woman.class), createHuman("father", Man.class)).prettyFormat());
                case 7 -> Screen.print(Boolean.toString(familyController.deleteFamilyByIndex(checkID())));
                case 8 -> addChildMenu();
                case 9 -> familyController.deleteAllChildrenOlderThen(checkNumber("years old", 0, 123));
            }
        }
    }

    Map<Integer, String> ChildCreatorMap = new HashMap<>() {{
        put(1, "Born child");
        put(2, "Adopt child");
        put(3, "Back to main menu");
    }};

    public void addChildMenu() {
        for (Map.Entry<Integer, String> entry : ChildCreatorMap.entrySet()) {
            Screen.print(entry.getKey() + ": " + entry.getValue());
        }
        String userChoice = Screen.read();
        if (userChoice.matches("^[1-3]$") && ChildCreatorMap.containsKey(Integer.parseInt(userChoice)))
            doChildSelection(Integer.parseInt(userChoice));
    }

    public void doChildSelection(int selection) {
        switch (selection) {
            case 1 -> familyController.bornChild(familyController.getFamilyById(checkID()), checkText("boy name"), checkText("girl name"));
            case 2 -> familyController.adoptChild(familyController.getFamilyById(checkID()), childToBeAdopted());
            case 3 -> {}
        }
    }

    public String checkText(String s) {
        String text;
        do {
            Screen.print("Enter " + s + ": ");
            text = Screen.read();
        } while (!text.matches("[a-zA-Z]{2,20}"));
        return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
    }

    public int checkNumber(String s, int min, int max) {
        String textNumber;
        do {
            Screen.print("Enter " + s + " from " + min + " to " + max + ": ");
            textNumber = Screen.read();
        } while (!textNumber.matches("^[0-9]+$"));
        int number = Integer.parseInt(textNumber);
        if (number < min || number > max)
            number = checkNumber(s, min, max);
        return number;
    }

    public <T extends Human> T createHuman(String s, Class<T> clazz) {
        String name = checkText(s + "'s name");
        String surname = checkText(s + "'s surname");
        int year = checkNumber(s + "'s year of birth", 1900, 2023);
        int month = checkNumber(s + "'s month of birth", 1, 12);
        int day = checkNumber(s + "'s date of birth", 1, LocalDate.of(year, month, 1).lengthOfMonth());
        long birthDate = LocalDate.of(year, month, day).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        int iq = checkNumber(s + "'s iq", 1, 200);
        try {
            Constructor<T> constructor = clazz.getConstructor(String.class, String.class, long.class, int.class);
            return constructor.newInstance(name, surname, birthDate, iq);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public int checkID() {
        return checkNumber("index", 0, familyController.count() - 1);
    }

    public Human childToBeAdopted() {
        return isBoy()
                ? createHuman("Boy", Man.class)
                : createHuman("Girl", Woman.class);
    }

    public boolean isBoy() {
        String textNumber;
        do {
            Screen.print("Enter 1 for boy, or 2 for girl");
            textNumber = Screen.read();
        } while (!textNumber.matches("^[1-2]$"));
        return textNumber.equals("1");
    }

    public boolean isEmpty() {
        return familyController.count() == 0;
    }
}
