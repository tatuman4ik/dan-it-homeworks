package Main;

import DAO.FamilyService;
import Human.Family;
import Human.Human;
import Human.Man;
import Human.Woman;
import Pets.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController() {
        familyService = new FamilyService();
    }

    public FamilyController(int i) {
        familyService = new FamilyService(i);
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public void getFamiliesBiggerThan(int count) {
        familyService.getFamiliesBiggerThan(count);
    }

    public void getFamiliesLessThan(int count) {
        familyService.getFamiliesLessThan(count);
    }

    public long countFamiliesWithMemberNumber(int count) {
        return familyService.countFamiliesWithMemberNumber(count);
    }

    public Family createNewFamily(Woman mother, Man father) {
        return familyService.createNewFamily(mother, father);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        if (family.countFamily() >= Main.MAX_SIZE)
            throw new FamilyOverflowException();
        return familyService.bornChild(family, boyName, girlName);
    }

    public Family adoptChild(Family family, Human child) {
        if (family.countFamily() >= Main.MAX_SIZE)
            throw new FamilyOverflowException();
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }
}
