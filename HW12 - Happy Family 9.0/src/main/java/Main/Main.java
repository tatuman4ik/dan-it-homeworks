package Main;

public class Main {
    public final static int MAX_SIZE = 10;
    public static void main(String[] args) {
        Screen.print("Choose your data:\n1 - for working with Collection\ndefault - working with Local File");
        FamilyController familyController = Screen.read().contains("1")
                ? new FamilyController()
                : new FamilyController(0);
        StartMenu startMenu = new StartMenu(familyController);
    }
}
