package DAO;

import Human.Family;

import java.util.*;
import java.io.*;

public class LocalFamilyDao implements FamilyDao, Serializable{
    String filePath = System.getProperty("user.dir") + File.separator + "families.dat";
    private File file = new File(filePath);
    private List<Family> families;

    public LocalFamilyDao() {
        families = new ArrayList<>();
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch(IOException e) {
            throw new RuntimeException("Error while creating file", e);
        }
    }

    public List<Family> getAllFamilies() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            families = (List<Family>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException("Error while reading data form file", e);
        }
        return families;
    }

    public Family getFamilyByIndex(int index) {
        try {
            return families.get(index);
        } catch (IndexOutOfBoundsException e) {
            throw new RuntimeException("Error while getting Family by index", e);
        }
    }

    public boolean deleteFamily(Family family) {
        boolean isDeleted;
        isDeleted = families.remove(family);
        loadData(families);
        return isDeleted;
    }

    public boolean deleteFamily(int index) {
        boolean isDeleted;
        isDeleted = families.remove(families.get(index));
        loadData(families);
        return isDeleted;
    }

    public Family saveFamily(Family family) {
        if (!families.isEmpty() && families.contains(family)) {
            int index = families.indexOf(family);
            families.set(index, family);
        } else
            families.add(family);
        loadData(families);
        return family;
    }

    public void loadData(List<Family> familiesToLoad) {
         try {
             ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
             oos.writeObject(familiesToLoad);
        } catch (IOException e) {
            throw new RuntimeException("Error while loading data to file", e);
        }
    }
}
