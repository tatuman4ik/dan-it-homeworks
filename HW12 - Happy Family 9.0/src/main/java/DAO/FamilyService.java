package DAO;

import Human.Family;
import Human.Human;
import Human.Man;
import Human.Woman;
import Main.Screen;
import Pets.Pet;

import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService() {
        familyDao = new CollectionFamilyDao();
    }

    public FamilyService(int i) {
        familyDao = new LocalFamilyDao();
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();
        IntStream.range(0, families.size())
                .mapToObj(i -> (i + 1) + ". " + families.get(i).prettyFormat())
                .forEach(Screen::print);
    }

    public void getFamiliesBiggerThan(int count) {
        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() > count)
                .forEach(family -> Screen.print(family.prettyFormat()));
    }

    public void getFamiliesLessThan(int count) {
        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() < count)
                .forEach(family -> Screen.print(family.prettyFormat()));
    }

    public long countFamiliesWithMemberNumber(int count) {
        return familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() == count)
                .count();
    }

    public Family createNewFamily(Woman mother, Man father) {
        return familyDao.saveFamily(new Family(mother, father));
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        Human child = family.bornChild();
        child.setName(child instanceof Man ? boyName : girlName);
        familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyDao.getAllFamilies()
                .forEach(family -> {
                    family.getChildren().removeIf(child -> child.getAge() > age);
                    familyDao.saveFamily(family);
                    });
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index) {
        return familyDao.getFamilyByIndex(index).getPets();
    }

    public void addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        family.setPet(pet);
        familyDao.saveFamily(family);
    }
}
