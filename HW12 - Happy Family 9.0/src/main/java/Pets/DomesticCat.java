package Pets;

import Main.Screen;

import java.util.HashSet;
import java.util.Set;

public class DomesticCat extends Pet implements CanFoul{
    private boolean innocent = true;

    public DomesticCat(String nickname, int age, int trickLevel, Set<Habits> habits, boolean hungry) {
        super(nickname, age, trickLevel, habits, hungry);
        setSpecies(Species.CAT);
    }

    public DomesticCat(String nickname, int age, Set<Habits> habits) {
        super(nickname, age, 0, habits, false);
        setSpecies(Species.CAT);
    }

    public DomesticCat(String nickname) {
        super(nickname, 0, 0, new HashSet<>(), false);
        setSpecies(Species.CAT);
    }

    public DomesticCat() {
        super(null, 0, 0, new HashSet<>(), false);
        setSpecies(Species.CAT);
    }


    public void eat() {
        Screen.print("Мур-мур! Як смачно!");
        setHungry(false);
    }

    public void foul() {
        Screen.print("Треба добре замісти сліди...");
        int level = getTrickLevel();
        if (isLucky()) {
            setInnocent(true);
            if (level < 100) level++;
        } else {
            setInnocent(false);
            if (level > 0) level--;
        }
        setTrickLevel(level);
    }

    public void setInnocent(boolean innocent) {
        this.innocent = innocent;
    }

    public boolean isInnocent() {
        return innocent;
    }

}
