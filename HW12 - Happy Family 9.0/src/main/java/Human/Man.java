package Human;

import Main.Screen;
import Pets.Pet;
import Schedule.Schedule;

import java.time.Instant;

public final class Man extends Human {

    public Man(String name, String surname, long birthDate, int iq, Schedule schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Man(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, int iq, Family family) {
        super(name, surname, Instant.now().toEpochMilli(), iq);
        setFamily(family);
    }
    public Man(String name, String surname, long birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Man() {
        super(null, null, Instant.now().toEpochMilli());
    }
    
    @Override
    public void greetPet() {
        if (getFamily() != null && !getFamily().getPets().isEmpty()) {
            for(Pet pet : getFamily().getPets()) {
                Screen.print("Привіт, " + pet.getNickname() + ", давай гратися!");
                pet.respond(this);
            }
        }
    }

    public void repairCar() {
        Screen.print(getName() + " пішов до сервісу");
    }
}
