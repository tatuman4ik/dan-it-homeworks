package Human;

import Pets.Dog;
import Pets.DomesticCat;
import Pets.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    private Man NotNullMan, EmptyFieldMan, NullMan;
    private Woman NotNullWoman, EmptyFieldWoman, NullWoman;
    private DomesticCat Cat;
    private Pets.Dog Dog;
    private Pet NullPet;

    @BeforeEach
    public void setup() {
        NotNullMan = new Man("John", "Stevenson", 1678492800);
        NotNullWoman = new Woman("Mary", "Johnson", 1678402800);
        EmptyFieldMan = new Man();
        EmptyFieldWoman = new Woman();
        NullMan = null;
        NullWoman = null;
        Cat = new DomesticCat("Cat");
        Dog = new Dog("Pets.Dog");
        NullPet = null;
    }

    @Test
    void constructorWithoutArgumentsShouldThrowException() {
        assertThrows(
                IllegalArgumentException.class,
                Family::new
        );
    }

    @Test
    void constructorWith2NullArgumentsShouldThrowException() {
        assertNull(NullMan);
        assertNull(NullWoman);

        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NullWoman, NullMan)
        );
    }

    @Test
    void constructorWithNullAndNotNullArgumentsShouldThrowException() {
        assertNull(NullMan);
        assertNotNull(NotNullMan);
        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NullWoman, NotNullMan)
        );

        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NotNullWoman, NullMan)
        );
    }

    @Test
    void constructorWith2DifferentArgumentsShouldExecute() {
        assertNotEquals(
                NotNullMan,
                NotNullWoman
        );
        assertDoesNotThrow(
                () -> new Family(NotNullWoman, NotNullMan)
        );
    }

    @Test
    void constructorWith2NotNullDifferentArgumentsButEqualFamilyShouldThrowException() {
        assertNotEquals(
                NotNullMan,
                NotNullWoman
        );
        Family family = new Family(NotNullWoman, NotNullMan);

        assertEquals(
                family,
                NotNullMan.getFamily()
        );
        assertEquals(
                family,
                NotNullWoman.getFamily()
        );

        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NotNullWoman, NotNullMan)
        );
    }

    @Test
    void getMotherShouldReturnMotherWoman() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertEquals(
                NotNullMan.getFamily(),
                NotNullWoman.getFamily()
        );

        assertEquals(
                NotNullWoman,
                family.getMother()
        );

        assertNotEquals(
                NotNullMan,
                family.getMother()
        );
    }

    @Test
    void getFatherShouldReturnFatherMan() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertEquals(
                NotNullMan.getFamily(),
                NotNullWoman.getFamily()
        );

        assertEquals(
                NotNullMan,
                family.getFather()
        );

        assertNotEquals(
                NotNullWoman,
                family.getFather()
        );
    }

    @Test
    void addChildShouldAddChildToFamily() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertNotEquals(
                family,
                EmptyFieldMan.getFamily()
        );

        int beforeLength = family.getChildren().size();

        assertDoesNotThrow(
                () -> family.addChild(EmptyFieldMan)
        );

        int afterLength = family.getChildren().size();

        assertEquals(
                beforeLength,
                afterLength - 1
        );
        assertEquals(
                family,
                EmptyFieldMan.getFamily()
        );
        assertTrue(
                family.getChildren().contains(EmptyFieldMan)
        );
    }

    @Test
    void addChildShouldNotSetParent1ToChildren() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertEquals(
                family.getMother(),
                NotNullWoman
        );
        int beforeLength = family.getChildren().size();

        assertDoesNotThrow(
                () -> family.addChild(NotNullWoman)
        );
        int afterLength = family.getChildren().size();

        assertEquals(
                beforeLength,
                afterLength
        );
        assertFalse(
                family.getChildren().contains(NotNullWoman)
        );
    }

    @Test
    void addChildShouldNotSetParent2ToChildren() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertEquals(
                family.getFather(),
                NotNullMan
        );
        int beforeLength = family.getChildren().size();

        assertDoesNotThrow(
                () -> family.addChild(NotNullMan)
        );
        int afterLength = family.getChildren().size();

        assertEquals(
                beforeLength,
                afterLength
        );
        assertFalse(
                family.getChildren().contains(NotNullMan)
        );
    }

    @Test
    void addChildNotAddTwiceIfAlreadyHasThisChild() {
        Family family = new Family(NotNullWoman, NotNullMan);
        family.addChild(EmptyFieldMan);

        assertTrue(
                family.getChildren().contains(EmptyFieldMan)
        );

        int beforeLength = family.getChildren().size();

        assertDoesNotThrow(
                () -> family.addChild(EmptyFieldMan)
        );

        int afterLength = family.getChildren().size();

        assertTrue(
                family.getChildren().contains(EmptyFieldMan)
        );
        assertEquals(
                beforeLength,
                afterLength
        );
    }

    @Test
    void deleteChildObjectFamilyTrue() {
        Family family = new Family(NotNullWoman, NotNullMan);
        family.addChild(EmptyFieldMan);

        assertTrue(
                family.getChildren().contains(EmptyFieldMan)
        );

        int beforeLength = family.getChildren().size();

        assertTrue(
                family.deleteChild(EmptyFieldMan)
        );

        int afterLength = family.getChildren().size();

        assertFalse(
                family.getChildren().contains(EmptyFieldMan)
        );
        assertEquals(
                beforeLength - 1,
                afterLength
        );
    }

    @Test
    void deleteChildObjectFamilyFalse() {
        Family family = new Family(NotNullWoman, NotNullMan);
        family.addChild(EmptyFieldMan);

        assertFalse(
                family.getChildren().contains(EmptyFieldWoman)
        );

        int beforeLength = family.getChildren().size();

        assertFalse(
                family.deleteChild(EmptyFieldWoman)
        );

        int afterLength = family.getChildren().size();

        assertFalse(
                family.getChildren().contains(EmptyFieldWoman)
        );
        assertEquals(
                beforeLength,
                afterLength
        );

    }

    @Test
    void deleteChildIndexFromChildrenArrayFamilyTrue() {
        Family family = new Family(NotNullWoman, NotNullMan);
        family.addChild(EmptyFieldMan);

        assertTrue(
                family.getChildren().contains(EmptyFieldMan)
        );

        int beforeLength = family.getChildren().size();

        assertTrue(
                family.deleteChild(0)
        );

        int afterLength = family.getChildren().size();

        assertFalse(
                family.getChildren().contains(EmptyFieldMan)
        );
        assertEquals(
                beforeLength - 1,
                afterLength
        );
    }

    @Test
    void deleteChildIndexFromChildrenArrayFamilyFalse() {
        Family family = new Family(NotNullWoman, NotNullMan);
        family.addChild(EmptyFieldMan);

        int beforeLength = family.getChildren().size();

        assertFalse(
                family.deleteChild(1)
        );

        int afterLength = family.getChildren().size();

        assertEquals(
                beforeLength,
                afterLength
        );

    }

    @Test
    void countFamilyEqual2ParentsWithoutAnyChild() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertEquals(
                2,
                family.countFamily()
        );

        assertThrows(
                IllegalArgumentException.class,
                () -> family.addChild(NullMan)
        );
        assertEquals(
                2,
                family.countFamily()
        );
    }

    @Test
    void countFamilyEqual2ParentsAndChildrenArrayListSize() {
        Family family = new Family(NotNullWoman, NotNullMan);
        List<Human> children = new ArrayList<>(Arrays.asList(EmptyFieldMan, EmptyFieldWoman));

        int beforeLength = family.countFamily();

        assertDoesNotThrow(
                () -> { for (Human child : children) family.addChild(child); }
        );
        assertEquals(
                beforeLength + children.size(),
                family.countFamily()
        );

        beforeLength = family.countFamily();

        assertDoesNotThrow(
                () -> family.deleteChild(EmptyFieldMan)
        );

        assertEquals(
                beforeLength - 1,
                family.countFamily()
        );
    }

    @Test
    void getChildrenSizeEqual0WhenNoChildInFamily() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertEquals(
                0,
                family.getChildren().size()
        );

        assertEquals(
                0,
                family.getChildren().size()
        );
    }

    @Test
    void setPetItHasNoFamily() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertNull(
                Cat.getFamily()
        );

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void setPetItAlreadyHasFamily() {
        Family family1 = new Family(NotNullWoman, NotNullMan);
        Family family2 = new Family(EmptyFieldWoman, EmptyFieldMan);

        assertDoesNotThrow(
                () -> family1.setPet(Cat)
        );

        assertTrue(
                family1.getPets().contains(Cat)
        );

        assertFalse(
                family2.getPets().contains(Cat)
        );

        assertDoesNotThrow(
                () -> family2.setPet(Cat)
        );

        assertFalse(
                family1.getPets().contains(Cat)
        );

        assertTrue(
                family2.getPets().contains(Cat)
        );
    }

    @Test
    void setPetFamilyAlreadyHasOtherPet() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertFalse(
                family.getPets().contains(Dog)
        );

        assertDoesNotThrow(
                () -> family.setPet(Dog)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertTrue(
                family.getPets().contains(Dog)
        );
    }

    @Test
    void setPetDoNotAddNullPet() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertEquals(
                1,
                family.getPets().size()
        );

        assertNull(
                NullPet
        );

        assertDoesNotThrow(
                () -> family.setPet(NullPet)
        );

        assertEquals(
                1,
                family.getPets().size()
        );

        assertFalse(
                family.getPets().contains(NullPet)
        );
    }

    @Test
    void setPetItHasNoFamilyBooleanTrue() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertNull(
                Cat.getFamily()
        );

        assertDoesNotThrow(
                () -> family.setPet(Cat, true)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void setPetItAlreadyHasFamilyBooleanTrue() {
        Family family1 = new Family(NotNullWoman, NotNullMan);
        Family family2 = new Family(EmptyFieldWoman, EmptyFieldMan);

        assertDoesNotThrow(
                () -> family1.setPet(Cat, true)
        );

        assertTrue(
                family1.getPets().contains(Cat)
        );

        assertFalse(
                family2.getPets().contains(Cat)
        );

        assertDoesNotThrow(
                () -> family2.setPet(Cat, true)
        );

        assertTrue(
                family1.getPets().contains(Cat)
        );

        assertTrue(
                family2.getPets().contains(Cat)
        );
    }

    @Test
    void setPetFamilyAlreadyHasOtherPetBooleanTrue() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertDoesNotThrow(
                () -> family.setPet(Cat, true)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertFalse(
                family.getPets().contains(Dog)
        );

        assertDoesNotThrow(
                () -> family.setPet(Dog, true)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertTrue(
                family.getPets().contains(Dog)
        );
    }

    @Test
    void setPetNullWhenPetNullBooleanTrue() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertDoesNotThrow(
                () -> family.setPet(Cat, true)
        );

        assertEquals(
                1,
                family.getPets().size()
        );

        assertNull(
                NullPet
        );

        assertDoesNotThrow(
                () -> family.setPet(NullPet, true)
        );

        assertFalse(
                family.getPets().contains(NullPet)
        );
    }

    @Test
    void setPetItHasNoFamilyBooleanFalse() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertNull(
                Cat.getFamily()
        );

        assertDoesNotThrow(
                () -> family.setPet(Cat, false)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void setPetItAlreadyHasFamilyBooleanFalse() {
        Family family1 = new Family(NotNullWoman, NotNullMan);
        Family family2 = new Family(EmptyFieldWoman, EmptyFieldMan);

        assertDoesNotThrow(
                () -> family1.setPet(Cat, false)
        );

        assertTrue(
                family1.getPets().contains(Cat)
        );

        assertFalse(
                family2.getPets().contains(Cat)
        );

        assertDoesNotThrow(
                () -> family2.setPet(Cat, false)
        );

        assertTrue(
                family2.getPets().contains(Cat)
        );

        assertFalse(
                family1.getPets().contains(Cat)
        );
    }

    @Test
    void setPetFamilyAlreadyHasOtherPetBooleanFalse() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertDoesNotThrow(
                () -> family.setPet(Cat, false)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertFalse(
                family.getPets().contains(Dog)
        );

        assertDoesNotThrow(
                () -> family.setPet(Dog, false)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertTrue(
                family.getPets().contains(Dog)
        );
    }

    @Test
    void setPetNullWhenPetNullBooleanFalse() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertDoesNotThrow(
                () -> family.setPet(Cat, false)
        );

        assertEquals(
                1,
                family.getPets().size()
        );

        assertNull(
                NullPet
        );

        assertDoesNotThrow(
                () -> family.setPet(NullPet, false)
        );

        assertFalse(
                family.getPets().contains(NullPet)
        );
    }

    @Test
    void hasPetShouldTrueFamilyHasPet() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertEquals(
                1,
                family.getPets().size()
        );

        assertTrue(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void PetsShould0IfFamilyHasNotPet() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertEquals(
                0,
                family.getPets().size()
        );
    }

    @Test
    void deletePetRemovePetFromFamily() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertDoesNotThrow(
                () -> family.deletePet(Cat)
        );

        assertFalse(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void getPetShouldReturnPetFamilyHasPet() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void PetsShouldBeEmptyIfFamilyHasNoPet() {
        Family family = new Family(NotNullWoman, NotNullMan);

        assertTrue(
                family.getPets().isEmpty()
        );
    }
    @Test
    void toStringShouldBeEqualToExample() {
        Family family = new Family(NotNullWoman, NotNullMan);

        String example;
        example = "Family{parents:{name='Mary', surname='Johnson', age=53 years, 2 months, 25 days}, {name='John', surname='Stevenson', age=53 years, 2 months, 25 days}}}";
        assertEquals(
                example,
                family.toString()
        );
    }
    @Test
    void toStringShouldEmptyWhenEmpty() {
        Family family = new Family(EmptyFieldWoman, EmptyFieldMan);
        String example;
        example = "Family{parents:{name='null', surname='null', age=0 years, 0 months, 0 days}, {name='null', surname='null', age=0 years, 0 months, 0 days}}}";
        assertEquals(
                example,
                family.toString()
        );
    }
}