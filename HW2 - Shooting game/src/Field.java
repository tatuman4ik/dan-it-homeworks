public class Field {

    public int height;

    public int width;

    public int aims;

    public Cell[][] cells;

    public Field(int x, int y) {
        this.height = x;
        this.width = y;
        this.cells = new Cell[x][y];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                this.cells[i][j] = new Cell();
            }
        }
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public int getAims() {
        return this.aims;
    }

    public Cell getCells(int x, int y) {
        return this.cells[x][y];
    }

    public void setAims(int aims) {
        this.aims = aims;
    }

    public void arrangeAims() {
        int aimCount = 0;
        while (aimCount < getAims()) {
            int x = Method.randomNumber(getHeight());
            int y = Method.randomNumber(getWidth());
            if (! getCells(x, y).getAim()) {
                getCells(x, y).setAim();
                aimCount++;
            }
        }
    }
}
