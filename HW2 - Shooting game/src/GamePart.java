public class GamePart {
    public static Field start() {
        int x = Method.setSizes("Висота", 10);
        int y = Method.setSizes("Довжина", 10);
        Field field = new Field(x, y);
        int aims = Method.setSizes("Кількість цілей", x * y);
        field.setAims(aims);
        field.arrangeAims();
        Screen.print("Цілі розміщені на полі! Почнемо!\n");
        return field;
    }
    public static void shot(Field field) {
        int shotCounter = 0;
        int x, y;
        do {
            Screen.printField(field);
            x = Method.setShot("Оберіть рядок", 1, field.getHeight()) - 1;
            y = Method.setShot("Оберіть стовбчик", 1, field.getWidth()) - 1;
            field.getCells(x, y).shot();
            if (field.getCells(x, y).getAim()) shotCounter++;
        } while (shotCounter < field.getAims());
    }
    public static void end(Field field) {
        Screen.printField(field);
        Screen.print("Вітаю! Всі цілі знайдено!\n");
    }
}
