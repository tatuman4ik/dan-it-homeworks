public class Game {
    public static void main(String[] args) {
        while (true) {
            Field field = GamePart.start();
            GamePart.shot(field);
            GamePart.end(field);
        }
    }
}
