public class Cell {

    private int x;

    private int y;

    private boolean isAim = false;

    private boolean shooted = false;

    public Field field;

    public Cell() {}

    public void setAim() {
        this.isAim = true;
    }
    public void shot() {
        this.shooted = true;
    }
    public boolean getCondition() {return this.shooted; }
    public boolean getAim() { return this.isAim; }
    public int getX() {
        return this.x;
    }
    public int getY() {
        return this.y;
    }
    public void setEmpty() {
        this.isAim = false;
    }
    public Cell(Field field, int x, int y) {
        this.field = field;
        this.x = x;
        this.y = y;
    }
}
