import java.util.Scanner;

public class Screen {

    static Scanner scan = new Scanner(System.in);

    public static String read() {
        return scan.nextLine();
    }

    public static void print(String line) { System.out.printf("%s", line); }

    public static void error(String line) {
        System.err.printf("%s\n", line);
    }

    public static void printField(Field field) {
        for (int i = 0; i < field.getHeight(); i++) {
            if (i == 0) {
                for (int j = 0; j <= field.getWidth(); j++) Screen.print(" " + j + " |");
                Screen.print("\n" + "-".repeat(4 * (field.getWidth() + 1)));
            }
            Screen.print("\n " + (i + 1) + " |");
            for (int j = 0; j < field.getWidth(); j++) {
                if (field.getCells(i, j).getCondition()) {
                    if (field.getCells(i, j).getAim()) Screen.print(" X |");
                    else Screen.print(" * |");
                } else Screen.print("   |");
            }
            Screen.print("\n" + "-".repeat(4 * (field.getWidth() + 1)) + "\n");
        }
    }

    public static int enterNumber(int from, int till) {
        do {
            try {
                int number = Integer.parseInt(read());
                if (number >= from && number <= till)
                    return number;
                else
                    error ("Найбільший розмір - " + till);
            } catch (NumberFormatException exception) {
                error ("Тільки число!");
            }
        }while (true);
    }
}