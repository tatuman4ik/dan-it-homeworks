import java.util.Random;

public class Method {
    public static int setSizes(String length, int till) {
        Screen.print(length + " ігрового поля: ");
        return Screen.enterNumber(0, till);
    }
    public static int setShot(String length, int from, int till) {
        Screen.print(length + " для пострілу: ");
        return Screen.enterNumber(from, till);
    }
    public static int randomNumber(int till) {
        Random random = new Random();
        return random.nextInt(till);
    }
}
