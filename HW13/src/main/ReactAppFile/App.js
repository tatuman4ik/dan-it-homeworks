import React from 'react';
import UserList from './components/UserList';

const App = () => {
    return (
        <div>
            <h1>My React App</h1>
            <UserList/>
        </div>
    );
};

export default App;

