package app.dao;

import app.models.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public class AccountDao extends GenericDao<Account> {
    public AccountDao(JpaRepository<Account, Long> repository) {
        super(repository);
    }
}