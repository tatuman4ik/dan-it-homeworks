package app.service;

import app.dao.AccountDao;
import app.dao.CustomerDao;
import app.enums.Currency;
import app.models.Account;
import app.models.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerDao customerDao;
    private final AccountDao accountDao;

    public Customer getCustomerWithAccounts(Long customerId) {
        return customerDao.getOne(customerId);
    }

    public List<Customer> getAllCustomersWithAccounts() {
        return customerDao.findAll();
    }

    public Customer createCustomer(String name, String email, Integer age) {
        Customer newCustomer = new Customer(name, email, age);
        return customerDao.save(newCustomer);
    }

    public Customer updateCustomer(Long customerId, String name, String email, Integer age) {
        Customer existingCustomer = customerDao.getOne(customerId);
        if (existingCustomer != null) {
            existingCustomer.setName(name);
            existingCustomer.setEmail(email);
            existingCustomer.setAge(age);
            return customerDao.save(existingCustomer);
        }
        return null;
    }

    public void deleteCustomer(Long customerId) {
        Customer customer = customerDao.getOne(customerId);
        if (customer != null) {
            customerDao.delete(customer);
        }
    }

    public Account createAccount(Long customerId, Currency currency) {
        Customer customer = customerDao.getOne(customerId);
        if (customer != null) {
            Account newAccount = new Account(currency, customer);
            accountDao.save(newAccount);
            return newAccount;
        }
        return null;
    }

    public void deleteAccount(Long customerId, Long accountId) {
        Account account = accountDao.getOne(accountId);
        if (account != null && account.getCustomer().getId().equals(customerId)) {
            accountDao.delete(account);
        }
    }
}
