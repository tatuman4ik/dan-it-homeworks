package app.controller;

import app.service.AccountService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/accounts/{accountNumber}")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PutMapping("/deposit")
    public void deposit(@PathVariable String accountNumber, @RequestParam double amount) {
        accountService.deposit(accountNumber, amount);
    }

    @PutMapping("/withdraw")
    public void withdraw(@PathVariable String accountNumber, @RequestParam double amount) {
        accountService.withdraw(accountNumber, amount);
    }

    @PutMapping("/transfer/{toAccountNumber}")
    public void transfer(@PathVariable String accountNumber, @PathVariable String toAccountNumber, @RequestParam double amount) {
        accountService.transfer(accountNumber, toAccountNumber, amount);
    }
}
