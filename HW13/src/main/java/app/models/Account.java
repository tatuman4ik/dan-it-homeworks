package app.models;

import app.enums.Currency;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "Accounts")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ac_id", unique = true, nullable = false)
    private Long id;

    @Column(name = "ac_uuid")
    @JsonIgnore
    private String number = UUID.randomUUID().toString();

    @Column(name = "currency")
    private Currency currency;

    @Column(name = "balance")
    private Double balance = 0.0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Customer customer;

    public Account(@NonNull Currency currency, @NonNull Customer customer) {
        this.currency = currency;
        this.customer = customer;
    }

}
