public class Schedule {
    private final int days = Week.DAYS_IN_WEEK;
    private String[][] schedule = new String[days][2];
    private TaskList tasks = new TaskList();
    private Week week = new Week();

    public void setWeekTasks(){
        for (int i = 0; i < days; i++) {
            schedule[i][0] = week.getDayName(i);
            schedule[i][1] = tasks.getTask(i);
        }
    }

    public void setOneTask(int dayNum) {
        schedule[dayNum][1] = tasks.setTask(dayNum);
    }

    public void printDay(int dayNum) {
        String dayName = schedule[dayNum][0];
        Screen.print("\n");
        Screen.print(dayName.toUpperCase());
        Screen.print("-".repeat(dayName.length()));
    }

    public void printTask(int dayNum) {
        String dayTask = schedule[dayNum][1];
        Screen.print(dayTask);
    }
    public boolean equals(String find) {
        Screen.error("Not implemented yet");
        return false;
    }

    public int getWeekLength() {
        return days;
    }
}
