public class Menu {
    public int chooseDay() {
        Screen.print("\nУведіть назву дня, щоб прочитати нотатки, або \"вихід\" для завершення роботи");
        while (true) {
            String input = Screen.read().trim().toLowerCase();
            switch (input) {
                case "понеділок": return 0;
                case "вівторок": return 1;
                case "середа": return 2;
                case "четверг": return 3;
                case "п'ятниця": return 4;
                case "субота": return 5;
                case "неділя": return 6;
                case "вихід": return 7;
                default: Screen.error("Спробуйте ще раз");
            }
        }
    }

    public boolean whatToDo() {
        Screen.print("\nУведіть \"змінити\" для зміни нотатки або \"назад\" для повернення в меню");
        while (true) {
            String input = Screen.read().trim().toLowerCase();
            switch (input) {
                case "змінити": return true;
                case "назад": return false;
                default: Screen.error("Спробуйте ще раз");
            }
        }
    }
}
