public class TaskList {
    private final int daysInWeek = Week.DAYS_IN_WEEK;
    private String[] tasks;

    public TaskList() {
        tasks = new String[daysInWeek];
        weekTasks();
    }

    private void weekTasks() {
        tasks[0] = "Початок проєкту. Мітінг і обговорення. Обід о 13:00";
        tasks[1] = "Розробка плану виконання. Уточнення деталей. Обід о 13:00";
        tasks[2] = "Зосереджена праця. Обід о 13:00";
        tasks[3] = "Виявлення помилок. Знайти і покарати винних. Піти додому о 13:00";
        tasks[4] = "Дедлайн по проєкту. Взяти ящик Редбулу. Замовити піцу в офіс";
        tasks[5] = "Гіт коміт -> Гіт пуш. Випити кави і поїхати додому. Виключити телефон";
        tasks[6] = "Купити біту з написом \"А я вам січас покажу...\". Включити телефон";
    }

    public String getTask(int taskNum) {
        return tasks[taskNum];
    }

    public String setTask(int dayNum) {
        Screen.print("Уведіть свої нотатки");
        tasks[dayNum] = Screen.read();
        return tasks[dayNum];
    }
}
