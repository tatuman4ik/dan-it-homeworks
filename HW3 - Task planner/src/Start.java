public class Start {
    public static void main(String[] args) {

        Schedule schedule = new Schedule();
        schedule.setWeekTasks();
        boolean toChange = true;
        Menu organiser = new Menu();
        while (toChange) {
            int dayNum = organiser.chooseDay();
            if (dayNum != 7) {
                schedule.printDay(dayNum);
                schedule.printTask(dayNum);
                if (organiser.whatToDo()) {
                    schedule.setOneTask(dayNum);
                }
            } else toChange = false;
        }
    }
}
