public class Week {
    final static int DAYS_IN_WEEK = 7;
    private String[] daysOfWeek;

    public Week() {
        daysOfWeek = new String[DAYS_IN_WEEK];
        setWeek();
    }

    private void setWeek() {
        daysOfWeek[0] = "Понеділок";
        daysOfWeek[1] = "Вівторок";
        daysOfWeek[2] = "Середа";
        daysOfWeek[3] = "Четверг";
        daysOfWeek[4] = "Пʼятниця";
        daysOfWeek[5] = "Субота";
        daysOfWeek[6] = "Неділя";
    }
    public String getDayName(int dayNum) {
        return daysOfWeek[dayNum];
    }

    private int getLength() {
        return DAYS_IN_WEEK;
    }

    public boolean compare(String input) {
        Screen.error("Not implemented yet");
        return false;
    }
}
