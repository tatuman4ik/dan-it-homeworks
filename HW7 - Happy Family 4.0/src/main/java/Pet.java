import java.util.*;

public abstract class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<Habits> habits;
    private boolean hungry;
    private Family family;

    public Pet(String nickname, int age, int trickLevel, Set<Habits> habits, boolean hungry) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.hungry = hungry;
    }

    public Pet(String nickname, int age, Set<Habits> habits) {
        this(nickname, age, 0, habits, false);
    }

    public Pet(String nickname) {
        this(nickname, 0, 0, new HashSet<>(), false);
    }

    public Pet() {}

    public void eat() {}

    public void respond(Human human) {
        Screen.print("Привіт, хазяїн " + human.getName() + "! Я " + getNickname() + ". Я скучив!");
    }

    public boolean isLucky() {
        Random random = new Random();
        if (getTrickLevel() > random.nextInt(101))
            return true;
        return false;
    }

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder();
        if (species != null) message.append(species);
        message.append("{");
        if (nickname != null) message.append("nickname='").append(nickname).append("'");
        message.append(", age=").append(age).append(", trickLevel=").append(trickLevel);
        if (species != null) {
            message.append(", canFly=").append(species.canFly());
            message.append(", numberOfLegs=").append(species.getNumberOfLegs());
            message.append(", hasFur=").append(species.hasFur());
        }
        if (!habits.isEmpty()) {
            message.append(", habits=[");
            Iterator<Habits> habitsIterator = habits.iterator();
            while (habitsIterator.hasNext()) {
                message.append(habitsIterator.next().name());
                if (habitsIterator.hasNext()) message.append(", ");
            }
            message.append("]");
        }
        message.append("}");
        return message.toString();
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setSpecies(Species species) {
        this.species = species != null ? species : Species.UNKNOWN;
    }

    public Species getSpecies() {
        return species;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setHabits(Set<Habits> habits) {
        this.habits = habits;
    }

    public Set<Habits> getHabits() {
        return habits;
    }

    public void setHungry(boolean hungry) {
        this.hungry = hungry;
    }

    public boolean isHungry() {
        return hungry;
    }

    public void setFamily(Family family) {
        setFamily(family, false);
    }

    public void setFamily(Family family, boolean isFreePet) {
        if (family != null) {
            if (!isFreePet) {
                if (this.hasFamily()) this.family.deletePet(this);
                this.family = null;
                family.setPet(this, true);
            }
        }
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public boolean hasFamily() {
        return this.family != null;
    }

    public void deleteFamily() {
        this.family = null;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        if (this == o) return true;
        Pet pet = (Pet) o;
        if (hashCode() != pet.hashCode()) return false;
        return age == pet.age &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname) &&
                Objects.equals(trickLevel, pet.trickLevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Об'єкт Pet буде видалений: " + this.toString());
        } finally {
            super.finalize();
        }
    }
}
