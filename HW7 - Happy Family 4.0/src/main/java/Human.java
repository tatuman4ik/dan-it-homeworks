import java.time.Year;
import java.util.*;

public class Human {
    private String name;
    private String surname;
    private Year year;
    private int iq;
    private Schedule schedule;
    private Family family;

    public Human(String name, String surname, Year year, int iq, Schedule schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, Year year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = 0;
        this.schedule = new Schedule();
    }

    public Human() {
        this(null, null, null);
    }

    public void greetPet() {
        if (getFamily() != null && !getFamily().getPets().isEmpty()) {
            for(Pet pet : getFamily().getPets()) {
                Screen.print("Привіт, " + pet.getNickname() + "!");
                pet.respond(this);
            }
        }
    }

    public void describePet() {
        if (getFamily() != null && !getFamily().getPets().isEmpty()) {
            for(Pet pet : getFamily().getPets()) {
                String very = (pet.getTrickLevel() > 50) ? " дуже " : " майже не ";
                String message = "У мене є " + pet.getSpecies() + " віком " +
                        pet.getAge() + " років, і він" + very + "хитрий.";
                Screen.print(message);
            }
        }
    }

    public boolean feedPet(Pet pet, boolean isTimeToFeed) {
        if (getFamily() == null || !getFamily().getPets().contains(pet)) return false;
        if (!isTimeToFeed && !pet.isLucky()) {
                Screen.print("Думаю, " + pet.getNickname() + " не голодний.");
                return false;
            }
        Screen.print("Хм... треба покормити " + pet.getNickname());
        pet.eat();
        return true;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb
                .append("Human{name='")
                .append(name)
                .append(", surname='")
                .append(surname)
                .append(", year=")
                .append(year)
                .append(", iq=")
                .append(iq)
                .append(", schedule=[")
                .append(schedule)
                .append("]}")
                .toString();
    }

    public String getHumanFieldsInfo() {
        String message = getClass().getSimpleName() + "{";
        if (getName() != null) message += "name='" + getName() + "'";
        if (getSurname() != null) message += ", surname='" + getSurname() + "'";
        if (getYear() != null) message += ", year=" + getYear();
        if (getIq() > 0) message += ", iq=" + getIq();
        message += "}";
        return message;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setYear(Year year) {
        this.year = year;
    }

    public Year getYear() {
        return year;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public int getIq() {
        return iq;
    }

    public void setFamily(Family family) {
        if (this.family != null && this.family.getChildren().contains(this))
            this.family.deleteChild(this);
        this.family = family;
        if (family != null && !this.equals(family.getParent1()) && !this.equals(family.getParent2())
                && !family.getChildren().contains(this))
            this.family.addChild(this);
    }

    public Family getFamily() {
        return family;
    }

    public void setSchedule(WeekDays oneDay, Tasks oneTask) {
        this.schedule.addTask(oneDay, oneTask);
    }

    public Schedule getSchedule() {
        return schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        if (this == o) return true;
        Human human = (Human) o;
        if (this.hashCode() != human.hashCode()) return false;
        return Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(year, human.year) &&
                Objects.equals(iq, human.iq) &&
                Objects.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq, schedule);
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Об'єкт Human буде видалений: " + this.toString());
        } finally {
            super.finalize();
        }
    }

}
