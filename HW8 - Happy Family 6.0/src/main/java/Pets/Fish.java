package Pets;

import Main.Screen;

import Human.Human;

import java.util.HashSet;
import java.util.Set;

public class Fish extends Pet{

    public Fish(String nickname, int age, int trickLevel, Set<Habits> habits, boolean hungry) {
        super(nickname, age, trickLevel, habits, hungry);
        setSpecies(Species.FISH);
    }

    public Fish(String nickname, int age, Set<Habits> habits) {
        super(nickname, age, 0, habits, false);
        setSpecies(Species.FISH);
    }

    public Fish(String nickname) {
        super(nickname, 0, 0, new HashSet<>(), false);
        setSpecies(Species.FISH);
    }

    public Fish() {
        super(null, 0, 0, new HashSet<>(), false);
        setSpecies(Species.FISH);
    }

    public void eat() {
        Screen.print("...:. .:.. .!");
        setHungry(false);
    }

    public void respond(Human human) {
        Screen.print(".:.:. " + human.getName() + "! .:. " + getNickname() + "! ..: .:..!");
    }

}
