package DAO;

import Human.*;
import Main.Screen;
import Pets.*;

import java.util.List;
import java.util.Set;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService() {
        familyDao = new CollectionFamilyDao();
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();
        for (int i = 0; i < families.size(); i++) {
            Screen.print(i + 1 + ". " + families.get(i));
        }
    }

    public void getFamiliesBiggerThan(int count) {
        for (Family family : familyDao.getAllFamilies()) {
            if (family.countFamily() > count) Screen.print(family.toString());
        }
    }

    public void getFamiliesLessThan(int count) {
        for (Family family : familyDao.getAllFamilies()) {
            if (family.countFamily() < count) Screen.print(family.toString());
        }
    }

    public int countFamiliesWithMemberNumber(int count) {
        int result = 0;
        for (Family family : familyDao.getAllFamilies()) {
            if (family.countFamily() == count) result++;
        }
        return result;
    }

    public Family createNewFamily(Human father, Human mother) {
        return familyDao.saveFamily(new Family(father, mother));
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        Human child = family.bornChild();
        child.setName(child instanceof Man ? boyName : girlName);
        familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        for (Family family : familyDao.getAllFamilies()) {
            family.getChildren().removeIf(child -> child.getAge() > age);
            familyDao.saveFamily(family);
        }
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index) {
        return familyDao.getFamilyByIndex(index).getPets();
    }

    public void addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        family.setPet(pet);
        familyDao.saveFamily(family);
    }
}
