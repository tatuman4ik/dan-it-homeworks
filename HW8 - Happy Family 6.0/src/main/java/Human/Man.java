package Human;

import Schedule.*;
import Pets.*;
import Main.Screen;

import java.time.Instant;

public final class Man extends Human {

    public Man(String name, String surname, long year, int iq, Schedule schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, long year) {
        super(name, surname, year, 0, new Schedule());
    }

    public Man(String name, String surname, int iq, Family family) {
        super(name, surname, Instant.now().toEpochMilli(), iq, new Schedule());
        setFamily(family);
    }

    public Man() {
        super(null, null, Instant.now().toEpochMilli());
    }
    
    @Override
    public void greetPet() {
        if (getFamily() != null && !getFamily().getPets().isEmpty()) {
            for(Pet pet : getFamily().getPets()) {
                Screen.print("Привіт, " + pet.getNickname() + ", давай гратися!");
                pet.respond(this);
            }
        }
    }

    public void repairCar() {
        Screen.print(getName() + " пішов до сервісу");
    }
}
