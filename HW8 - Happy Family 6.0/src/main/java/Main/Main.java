package Main;

import Human.*;
import Pets.*;

public class Main {
    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();
        Human Petro = new Human("Петро", "Коваленко", 229564800000L);
        Screen.print("Створив Петра з усіма полями. Ось він зараз:\n" + Petro);
        Human Marusya = new Human("Маруся", "Проценко", 261446400000L);
        Screen.print("Створив Марусю тільки з трьома полями. Ось вона зараз:\n" + Marusya);
        Family family1 = familyController.createNewFamily(Petro, Marusya);
        Marusya.setSurname(Petro.getSurname());
        Screen.print("\nСтворив нову сім'ю з Петром та Марусею, Марусі дав прізвище Петра:\n" + family1);
        Pet Murchyk = new DomesticCat("Мурчик");
        familyController.addPet(0, Murchyk);
        familyController.displayAllFamilies();
        familyController.getFamiliesBiggerThan(2);
        familyController.getFamiliesLessThan(3);
        Screen.print("Кількість сімей рівно з двома особами: " + familyController.countFamiliesWithMemberNumber(2));
        familyController.bornChild(familyController.getFamilyById(0), "Михайло", "Ніна");
        Human Taras = new Human("Тарас", "Ткачук", 967766400000L);
        familyController.adoptChild(family1, Taras);
        Screen.print("Зараз ця сім'я: " + family1);
        familyController.deleteAllChildrenOlderThen(18);
        Screen.print("Зараз сімей: " + familyController.count());
        Screen.print("Тварини: ");
        for (Pet pet : familyController.getPets(0)) Screen.print(pet.toString());
        familyController.deleteFamilyByIndex(0);
        familyController.displayAllFamilies();
        Screen.error("Кінець тесту.");
    }
}
