package Schedule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Schedule {
    private Map<WeekDays, List<Tasks>> schedule = new HashMap<>();

    public Schedule() {
        for (WeekDays oneDay : WeekDays.values())
            schedule.put(oneDay, new ArrayList<>());
    }

    public void addTask(WeekDays oneDay, Tasks oneTask) {
        schedule.get(oneDay).add(oneTask);
    }

    @Override
    public String toString() {
        StringBuilder taskList = new StringBuilder();
        for (WeekDays oneDay : schedule.keySet()) {
            List<Tasks> tasks = schedule.get(oneDay);
            if (!tasks.isEmpty()) {
                if (taskList.length() > 0) {
                    taskList.append(", ");
                }
                taskList.append("[");
                taskList.append(oneDay).append(": ");
                for (int i = 0; i < tasks.size(); i++) {
                    taskList.append(tasks.get(i).getDescription());
                    if (i < tasks.size() - 1) {
                        taskList.append(", ");
                    }
                }
                taskList.append("]");
            }
        }
        return taskList.toString();
    }
}
