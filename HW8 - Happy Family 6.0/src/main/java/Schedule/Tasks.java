package Schedule;

public enum Tasks {
    WORK("Work on project"),
    MEETING("Attend meeting"),
    GYM("Go to the gym"),
    CINEMA("Watch a movie"),
    CLEANING("Do some cleaning"),
    LAUNDRY("Do laundry"),
    PICNIC("Have a picnic"),
    STUDY("Study for exam"),
    DATE("Go on a date"),
    NOTHING("No task for today");

    private final String description;

    Tasks(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
