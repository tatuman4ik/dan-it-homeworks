package Main;

import Human.Family;
import Human.Human;
import Pets.Dog;
import Pets.DomesticCat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class FamilyControllerTest {
    private Human Human1, Human2, Human3, Human4;
    private Human FreeHuman1, FreeHuman2;
    private DomesticCat Cat;
    private Pets.Dog Dog;
    private FamilyController familyController;
    private Family family1, family2;

    @BeforeEach
    public void setup() {
        Human1 = new Human("John", "Stevenson", 997555655000L);
        Human2 = new Human("Mary", "Johnson", 987555655000L);
        Human3 = new Human("David", "Stevenson", 807562055000L);
        Human4 = new Human("Lisa", "Watson", 804562055000L);
        FreeHuman1 = new Human("1", "111", 966019655000L);
        FreeHuman2 = new Human("2", "222", 1078426055000L);
        Cat = new DomesticCat("Cat");
        Dog = new Dog("Pets.Dog");
        familyController = new FamilyController();
        family1 = familyController.createNewFamily(Human1, Human2);
        family2 = familyController.createNewFamily(Human3, Human4);
    }

    @Test
    void getAllFamiliesShouldGiveListOf2Families() {
        assertEquals(
                2,
                familyController.getAllFamilies().size()
        );
        assertEquals(
                family1,
                familyController.getAllFamilies().get(0)
        );
        assertEquals(
                family2,
                familyController.getAllFamilies().get(1)
        );
    }

    @Test
    void displayAllFamiliesShouldEqualExample() {
        String expectedOutput;
        expectedOutput = """
                1. Family{батьки:'{name='John', surname='Stevenson', age=21 years, 8 months, 1 days', '{name='Mary', surname='Johnson', age=21 years, 11 months, 25 days'}
                2. Family{батьки:'{name='David', surname='Stevenson', age=27 years, 8 months, 8 days', '{name='Lisa', surname='Watson', age=27 years, 9 months, 11 days'}
                """;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        familyController.displayAllFamilies();

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void getFamiliesBiggerThan1ShouldEqualExample() {
        String expectedOutput;
        expectedOutput = """
                Family{батьки:'{name='John', surname='Stevenson', age=21 years, 8 months, 1 days', '{name='Mary', surname='Johnson', age=21 years, 11 months, 25 days'}
                Family{батьки:'{name='David', surname='Stevenson', age=27 years, 8 months, 8 days', '{name='Lisa', surname='Watson', age=27 years, 9 months, 11 days'}
                """;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        familyController.getFamiliesBiggerThan(1);

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void getFamiliesLessThan3ShouldEqualExample() {
        String expectedOutput;
        expectedOutput = """
                Family{батьки:'{name='John', surname='Stevenson', age=21 years, 8 months, 1 days', '{name='Mary', surname='Johnson', age=21 years, 11 months, 25 days'}
                Family{батьки:'{name='David', surname='Stevenson', age=27 years, 8 months, 8 days', '{name='Lisa', surname='Watson', age=27 years, 9 months, 11 days'}
                """;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        familyController.getFamiliesLessThan(3);

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void countFamilies2MembersShouldEqual2() {
        assertEquals(
                2,
                familyController.countFamiliesWithMemberNumber(2)
        );
    }

    @Test
    void createNewFamilyShouldAddFamilyToLIst() {
        assertEquals(
                2,
                familyController.getAllFamilies().size()
        );
        assertDoesNotThrow(
                () -> familyController.createNewFamily(FreeHuman1, FreeHuman2)
        );
        assertEquals(
                2 + 1,
                familyController.getAllFamilies().size()
        );
    }

    @Test
    void deleteFamilyByIndexShouldDeleteFamily() {
        assertEquals(
                2,
                familyController.getAllFamilies().size()
        );
        assertDoesNotThrow(
                () -> familyController.createNewFamily(FreeHuman1, FreeHuman2)
        );
        assertEquals(
                2 + 1,
                familyController.getAllFamilies().size()
        );
        assertDoesNotThrow(
                () -> familyController.deleteFamilyByIndex(2)
        );
        assertEquals(
                2,
                familyController.getAllFamilies().size()
        );
    }

    @Test
    void bornChildShouldAdd1Child() {
        assertTrue(
                family1.getChildren().isEmpty()
        );
        assertTrue(
                family2.getChildren().isEmpty()
        );
        assertDoesNotThrow(
                () -> familyController.bornChild(family1, "Nash", "Rimma")
        );
        assertDoesNotThrow(
                () -> familyController.bornChild(family2, "Robert", "Anna")
        );
        assertEquals(
                1,
                family1.getChildren().size()
        );
        assertEquals(
                family1,
                familyController.getFamilyById(0)
        );
        assertEquals(
                1,
                family2.getChildren().size()
        );
        assertEquals(
                family2,
                familyController.getFamilyById(1)
        );
    }

    @Test
    void adoptChildShouldAddHumanToFamilyChildren() {
        assertTrue(
                family1.getChildren().isEmpty()
        );
        assertDoesNotThrow(
                () -> familyController.adoptChild(family1, FreeHuman1)
        );
        assertTrue(
                family1.getChildren().contains(FreeHuman1)
        );
    }

    @Test
    void deleteAllChildrenOlderThen18() {
        assertEquals(
                0,
                family1.getChildren().size()
        );
        assertDoesNotThrow(
                () -> familyController.adoptChild(family1, FreeHuman1)
        );
        assertDoesNotThrow(
                () -> familyController.bornChild(family1, "Nash", "Rimma")
        );
        assertEquals(
                2,
                family1.getChildren().size()
        );
        assertEquals(
                0,
                family2.getChildren().size()
        );
        assertDoesNotThrow(
                () -> familyController.adoptChild(family2, FreeHuman2)
        );
        assertDoesNotThrow(
                () -> familyController.bornChild(family2, "Nick", "Santa")
        );
        assertEquals(
                2,
                family2.getChildren().size()
        );
        assertDoesNotThrow(
                () -> familyController.deleteAllChildrenOlderThen(18)
        );
        Screen.print(family2.getChildren().get(0).toString());
        assertEquals(
                1,
                family2.getChildren().size()
        );
        assertEquals(
                1,
                family2.getChildren().size()
        );
    }

    @Test
    void countSholdBe2() {
        assertEquals(
                2,
                familyController.count()
        );
    }

    @Test
    void getFamilyByIdShouldEqualFamily() {
        assertEquals(
                family1,
                familyController.getFamilyById(0)
        );
        assertEquals(
                family2,
                familyController.getFamilyById(1)
        );
    }

    @Test
    void getPetsShouldGetListPets() {
        family1.setPet(Cat);
        family1.setPet(Dog);
        assertEquals(
                2,
                familyController.getPets(0).size()
        );
        assertTrue(
                familyController.getPets(0).contains(Cat)
        );
        assertTrue(
                familyController.getPets(0).contains(Dog)
        );
    }

    @Test
    void addPet() {
        assertEquals(
                0,
                familyController.getPets(0).size()
        );
        assertDoesNotThrow(
                () -> familyController.addPet(0, Cat)
        );
        assertEquals(
                1,
                familyController.getPets(0).size()
        );
        assertTrue(
                familyController.getPets(0).contains(Cat)
        );
        assertEquals(
                0,
                familyController.getPets(1).size()
        );
        assertDoesNotThrow(
                () -> familyController.addPet(1, Dog)
        );
        assertEquals(
                1,
                familyController.getPets(1).size()
        );
        assertTrue(
                familyController.getPets(1).contains(Dog)
        );
    }
}