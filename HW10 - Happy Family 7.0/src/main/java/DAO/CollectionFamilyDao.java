package DAO;

import Human.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao{
    private List<Family> families;

    public CollectionFamilyDao() {
        families = new ArrayList<>();
    }

    public Family saveFamily(Family family) {
        if (!families.contains(family)) families.add(family);
        return family;
    }

    public Family getFamilyByIndex(int index) {
        try {
            return families.get(index);
        } catch (IndexOutOfBoundsException e) {
            throw new RuntimeException("No such index", e);
        }
    }

    public List<Family> getAllFamilies() {
        return families;
    }

    public boolean deleteFamily(int index) {
        return deleteFamily(families.get(index));
    }

    public boolean deleteFamily(Family family) {
        return families.remove(family);
    }
}
