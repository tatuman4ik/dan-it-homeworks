package Human;

import Pets.Dog;
import Pets.DomesticCat;
import Pets.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    private Human NotNullHuman1;
    private Human NotNullHuman2;
    private Human EmptyFieldHuman1;
    private Human EmptyFieldHuman2;
    private Human NullHuman1;
    private Human NullHuman2;
    private DomesticCat Cat;
    private Pets.Dog Dog;
    private Pet NullPet;

    @BeforeEach
    public void setup() {
        NotNullHuman1 = new Human("John", "Stevenson", 1678492800);
        NotNullHuman2 = new Human("Mary", "Johnson", 1678402800);
        EmptyFieldHuman1 = new Human();
        EmptyFieldHuman2 = new Human();
        NullHuman1 = null;
        NullHuman2 = null;
        Cat = new DomesticCat("Cat");
        Dog = new Dog("Pets.Dog");
        NullPet = null;
    }

    @Test
    void constructorWithoutArgumentsShouldThrowException() {
        assertThrows(
                IllegalArgumentException.class,
                Family::new
        );
    }

    @Test
    void constructorWith2NullArgumentsShouldThrowException() {
        assertNull(NullHuman1);
        assertNull(NullHuman2);

        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NullHuman1, NullHuman2)
        );
    }

    @Test
    void constructorWithNullAndNotNullArgumentsShouldThrowException() {
        assertNull(NullHuman1);
        assertNotNull(NotNullHuman1);
        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NullHuman1, NotNullHuman1)
        );

        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NotNullHuman1, NullHuman1)
        );
    }

    @Test
    void constructorWith2EqualArgumentsShouldThrowException() {
        assertEquals(
                NotNullHuman1,
                NotNullHuman1
        );
        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NotNullHuman1, NotNullHuman1)
        );
    }

    @Test
    void constructorWith2DifferentArgumentsShouldExecute() {
        assertNotEquals(
                NotNullHuman1,
                NotNullHuman2
        );
        assertDoesNotThrow(
                () -> new Family(NotNullHuman1, NotNullHuman2)
        );
    }

    @Test
    void constructorWith2NotNullDifferentArgumentsButEqualFamilyShouldThrowException() {
        assertNotEquals(
                NotNullHuman1,
                NotNullHuman2
        );
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                family,
                NotNullHuman1.getFamily()
        );
        assertEquals(
                family,
                NotNullHuman2.getFamily()
        );

        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NotNullHuman1, NotNullHuman2)
        );
    }

    @Test
    void getParent1ShouldReturnFirstParent() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                NotNullHuman1.getFamily(),
                NotNullHuman2.getFamily()
        );

        assertEquals(
                NotNullHuman1,
                family.getParent1()
        );

        assertNotEquals(
                NotNullHuman2,
                family.getParent1()
        );
    }

    @Test
    void getParent2ShouldReturnSecondParent() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                NotNullHuman1.getFamily(),
                NotNullHuman2.getFamily()
        );

        assertEquals(
                NotNullHuman2,
                family.getParent2()
        );

        assertNotEquals(
                NotNullHuman1,
                family.getParent2()
        );
    }

    @Test
    void addChildShouldAddChildToFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertNotEquals(
                family,
                EmptyFieldHuman1.getFamily()
        );

        int beforeLength = family.getChildren().size();

        assertDoesNotThrow(
                () -> family.addChild(EmptyFieldHuman1)
        );

        int afterLength = family.getChildren().size();

        assertEquals(
                beforeLength,
                afterLength - 1
        );
        assertEquals(
                family,
                EmptyFieldHuman1.getFamily()
        );
        assertTrue(
                family.getChildren().contains(EmptyFieldHuman1)
        );
    }

    @Test
    void addChildShouldNotSetParent1ToChildren() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                family.getParent1(),
                NotNullHuman1
        );
        int beforeLength = family.getChildren().size();

        assertDoesNotThrow(
                () -> family.addChild(NotNullHuman1)
        );
        int afterLength = family.getChildren().size();

        assertEquals(
                beforeLength,
                afterLength
        );
        assertFalse(
                family.getChildren().contains(NotNullHuman1)
        );
    }

    @Test
    void addChildShouldNotSetParent2ToChildren() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                family.getParent2(),
                NotNullHuman2
        );
        int beforeLength = family.getChildren().size();

        assertDoesNotThrow(
                () -> family.addChild(NotNullHuman2)
        );
        int afterLength = family.getChildren().size();

        assertEquals(
                beforeLength,
                afterLength
        );
        assertFalse(
                family.getChildren().contains(NotNullHuman2)
        );
    }

    @Test
    void addChildNotAddTwiceIfAlreadyHasThisChild() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        assertTrue(
                family.getChildren().contains(EmptyFieldHuman1)
        );

        int beforeLength = family.getChildren().size();

        assertDoesNotThrow(
                () -> family.addChild(EmptyFieldHuman1)
        );

        int afterLength = family.getChildren().size();

        assertTrue(
                family.getChildren().contains(EmptyFieldHuman1)
        );
        assertEquals(
                beforeLength,
                afterLength
        );
    }

    @Test
    void hasThisChildShouldReturnTrueIfChildAlreadyChildThisFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        assertEquals(
                family,
                EmptyFieldHuman1.getFamily()
        );
        assertTrue(
                family.getChildren().contains(EmptyFieldHuman1)
        );
    }

    @Test
    void hasThisChildShouldReturnFalseIfChildNotThisFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertNotEquals(
                family,
                EmptyFieldHuman1.getFamily()
        );
        assertFalse(
                family.getChildren().contains(EmptyFieldHuman1)
        );
    }

    @Test
    void hasThisChildShouldReturnFalseIfChildIsParent() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                family.getParent1(),
                NotNullHuman1
        );
        assertFalse(
                family.getChildren().contains(NotNullHuman1)
        );

        assertEquals(
                family.getParent2(),
                NotNullHuman2
        );
        assertFalse(
                family.getChildren().contains(NotNullHuman2)
        );
    }

    @Test
    void deleteChildObjectFamilyTrue() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        assertTrue(
                family.getChildren().contains(EmptyFieldHuman1)
        );

        int beforeLength = family.getChildren().size();

        assertTrue(
                family.deleteChild(EmptyFieldHuman1)
        );

        int afterLength = family.getChildren().size();

        assertFalse(
                family.getChildren().contains(EmptyFieldHuman1)
        );
        assertEquals(
                beforeLength - 1,
                afterLength
        );
    }

    @Test
    void deleteChildObjectFamilyFalse() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        assertFalse(
                family.getChildren().contains(EmptyFieldHuman2)
        );

        int beforeLength = family.getChildren().size();

        assertFalse(
                family.deleteChild(EmptyFieldHuman2)
        );

        int afterLength = family.getChildren().size();

        assertFalse(
                family.getChildren().contains(EmptyFieldHuman2)
        );
        assertEquals(
                beforeLength,
                afterLength
        );

    }

    @Test
    void deleteChildIndexFromChildrenArrayFamilyTrue() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        assertTrue(
                family.getChildren().contains(EmptyFieldHuman1)
        );

        int beforeLength = family.getChildren().size();

        assertTrue(
                family.deleteChild(0)
        );

        int afterLength = family.getChildren().size();

        assertFalse(
                family.getChildren().contains(EmptyFieldHuman1)
        );
        assertEquals(
                beforeLength - 1,
                afterLength
        );
    }

    @Test
    void deleteChildIndexFromChildrenArrayFamilyFalse() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        int beforeLength = family.getChildren().size();

        assertFalse(
                family.deleteChild(1)
        );

        int afterLength = family.getChildren().size();

        assertEquals(
                beforeLength,
                afterLength
        );

    }

    @Test
    void countFamilyEqual2ParentsWithoutAnyChild() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                2,
                family.countFamily()
        );

        assertThrows(
                IllegalArgumentException.class,
                () -> family.addChild(NullHuman1)
        );
        assertEquals(
                2,
                family.countFamily()
        );
    }

    @Test
    void countFamilyEqual2ParentsAndChildrenArrayListSize() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        List<Human> children = new ArrayList<>(Arrays.asList(EmptyFieldHuman1, EmptyFieldHuman2));

        int beforeLength = family.countFamily();

        assertDoesNotThrow(
                () -> { for (Human child : children) family.addChild(child); }
        );
        assertEquals(
                beforeLength + children.size(),
                family.countFamily()
        );

        beforeLength = family.countFamily();

        assertDoesNotThrow(
                () -> family.deleteChild(EmptyFieldHuman1)
        );

        assertEquals(
                beforeLength - 1,
                family.countFamily()
        );
    }

    @Test
    void getChildrenSizeEqual0WhenNoChildInFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                0,
                family.getChildren().size()
        );

        assertEquals(
                0,
                family.getChildren().size()
        );
    }

    @Test
    void setPetItHasNoFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertNull(
                Cat.getFamily()
        );

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void setPetItAlreadyHasFamily() {
        Family family1 = new Family(NotNullHuman1, NotNullHuman2);
        Family family2 = new Family(EmptyFieldHuman1, EmptyFieldHuman2);

        assertDoesNotThrow(
                () -> family1.setPet(Cat)
        );

        assertTrue(
                family1.getPets().contains(Cat)
        );

        assertFalse(
                family2.getPets().contains(Cat)
        );

        assertDoesNotThrow(
                () -> family2.setPet(Cat)
        );

        assertFalse(
                family1.getPets().contains(Cat)
        );

        assertTrue(
                family2.getPets().contains(Cat)
        );
    }

    @Test
    void setPetFamilyAlreadyHasOtherPet() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertFalse(
                family.getPets().contains(Dog)
        );

        assertDoesNotThrow(
                () -> family.setPet(Dog)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertTrue(
                family.getPets().contains(Dog)
        );
    }

    @Test
    void setPetDoNotAddNullPet() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertEquals(
                1,
                family.getPets().size()
        );

        assertNull(
                NullPet
        );

        assertDoesNotThrow(
                () -> family.setPet(NullPet)
        );

        assertEquals(
                1,
                family.getPets().size()
        );

        assertFalse(
                family.getPets().contains(NullPet)
        );
    }

    @Test
    void setPetItHasNoFamilyBooleanTrue() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertNull(
                Cat.getFamily()
        );

        assertDoesNotThrow(
                () -> family.setPet(Cat, true)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void setPetItAlreadyHasFamilyBooleanTrue() {
        Family family1 = new Family(NotNullHuman1, NotNullHuman2);
        Family family2 = new Family(EmptyFieldHuman1, EmptyFieldHuman2);

        assertDoesNotThrow(
                () -> family1.setPet(Cat, true)
        );

        assertTrue(
                family1.getPets().contains(Cat)
        );

        assertFalse(
                family2.getPets().contains(Cat)
        );

        assertDoesNotThrow(
                () -> family2.setPet(Cat, true)
        );

        assertTrue(
                family1.getPets().contains(Cat)
        );

        assertTrue(
                family2.getPets().contains(Cat)
        );
    }

    @Test
    void setPetFamilyAlreadyHasOtherPetBooleanTrue() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat, true)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertFalse(
                family.getPets().contains(Dog)
        );

        assertDoesNotThrow(
                () -> family.setPet(Dog, true)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertTrue(
                family.getPets().contains(Dog)
        );
    }

    @Test
    void setPetNullWhenPetNullBooleanTrue() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat, true)
        );

        assertEquals(
                1,
                family.getPets().size()
        );

        assertNull(
                NullPet
        );

        assertDoesNotThrow(
                () -> family.setPet(NullPet, true)
        );

        assertFalse(
                family.getPets().contains(NullPet)
        );
    }

    @Test
    void setPetItHasNoFamilyBooleanFalse() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertNull(
                Cat.getFamily()
        );

        assertDoesNotThrow(
                () -> family.setPet(Cat, false)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void setPetItAlreadyHasFamilyBooleanFalse() {
        Family family1 = new Family(NotNullHuman1, NotNullHuman2);
        Family family2 = new Family(EmptyFieldHuman1, EmptyFieldHuman2);

        assertDoesNotThrow(
                () -> family1.setPet(Cat, false)
        );

        assertTrue(
                family1.getPets().contains(Cat)
        );

        assertFalse(
                family2.getPets().contains(Cat)
        );

        assertDoesNotThrow(
                () -> family2.setPet(Cat, false)
        );

        assertTrue(
                family2.getPets().contains(Cat)
        );

        assertFalse(
                family1.getPets().contains(Cat)
        );
    }

    @Test
    void setPetFamilyAlreadyHasOtherPetBooleanFalse() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat, false)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertFalse(
                family.getPets().contains(Dog)
        );

        assertDoesNotThrow(
                () -> family.setPet(Dog, false)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertTrue(
                family.getPets().contains(Dog)
        );
    }

    @Test
    void setPetNullWhenPetNullBooleanFalse() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat, false)
        );

        assertEquals(
                1,
                family.getPets().size()
        );

        assertNull(
                NullPet
        );

        assertDoesNotThrow(
                () -> family.setPet(NullPet, false)
        );

        assertFalse(
                family.getPets().contains(NullPet)
        );
    }

    @Test
    void hasPetShouldTrueFamilyHasPet() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertEquals(
                1,
                family.getPets().size()
        );

        assertTrue(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void PetsShould0IfFamilyHasNotPet() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                0,
                family.getPets().size()
        );
    }

    @Test
    void deletePetRemovePetFromFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );

        assertDoesNotThrow(
                () -> family.deletePet(Cat)
        );

        assertFalse(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void getPetShouldReturnPetFamilyHasPet() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertTrue(
                family.getPets().contains(Cat)
        );
    }

    @Test
    void PetsShouldBeEmptyIfFamilyHasNoPet() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertTrue(
                family.getPets().isEmpty()
        );
    }
    @Test
    void toStringShouldBeEqualToExample() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        String example;
        example = "Family{батьки:'{name='John', surname='Stevenson', age=53 years, 2 months, 23 days', '{name='Mary', surname='Johnson', age=53 years, 2 months, 23 days'}";

        assertEquals(
                example,
                family.toString()
        );
    }
    @Test
    void toStringShouldEmptyWhenEmpty() {
        Family family = new Family(EmptyFieldHuman1, EmptyFieldHuman2);
        String example;
        example = "Family{батьки:'{name='null', surname='null', age=0 years, 0 months, 0 days', '{name='null', surname='null', age=0 years, 0 months, 0 days'}";

        assertEquals(
                example,
                family.toString()
        );
    }
}