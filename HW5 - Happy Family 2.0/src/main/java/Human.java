import java.time.Year;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private Year year;
    private int iq;
    private Schedule schedule;
    private Family family;

    public Human(String name, String surname, Year year, int iq, Schedule schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, Year year) {
        this(name, surname, year, 0, new Schedule());
    }

    public Human() {
        this(null, null, null);
    }

    public void greetPet() {
        if (getFamily() != null && getFamily().getPet() != null) {
            String message = "Привіт, " + getFamily().getPet().getNickname() + "!";
            Screen.print(message);
            getFamily().getPet().respond(this);
        }
    }

    public void describePet() {
        if (getFamily() != null && getFamily().getPet() != null) {
            String very = (getFamily().getPet().getTrickLevel() > 50) ? " дуже " : " майже не ";
            String message = "У мене є " + getFamily().getPet().getSpecies() + " віком " +
                    getFamily().getPet().getAge() + " років, і він" + very + "хитрий.";
            Screen.print(message);
        }
    }

    public void feedPet() {
        if (getFamily().getPet() != null) getFamily().getPet().eat();
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (getFamily() == null || getFamily().getPet() == null) return false;
        if (!isTimeToFeed && !getFamily().getPet().isLucky()) {
                Screen.print("Думаю, " + getFamily().getPet().getNickname() + " не голодний.");
                return false;
            }
        Screen.print("Хм... треба покормити " + getFamily().getPet().getNickname());
        getFamily().getPet().eat();
        return true;
    }


        @Override
    public String toString() {
        String message = getClass().getSimpleName();
        message += (getName() != null) ? "{name='" + getName() : "{name='null";
        message += (getSurname() != null) ? "', surname='" + getSurname() : "', surname='null";
        message += (getYear() != null) ? "', year=" + getYear() : "', year=null";
        message += ", iq=" + getIq() + ", schedule=[" + getSchedule() + "]}";
        return  message;
    }

    public String getHumanFieldsInfo() {
        String message = getClass().getSimpleName() + "{";
        if (getName() != null) message += "name='" + getName() + "'";
        if (getSurname() != null) message += ", surname='" + getSurname() + "'";
        if (getYear() != null) message += ", year=" + getYear();
        if (getIq() > 0) message += ", iq=" + getIq();
        message += "}";
        return message;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setYear(Year year) {
        this.year = year;
    }

    public Year getYear() {
        return year;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public int getIq() {
        return iq;
    }

    public void setFamily(Family family) {
        if (this.family != null && this.family.hasThisChild(this)) this.family.deleteChild(this);
        this.family = family;
        if (family != null && !this.equals(family.getParent1()) &&
                !this.equals(family.getParent2()) && !family.hasThisChild(this)) {
            this.family.addChild(this);
        }
    }

    public Family getFamily() {
        return family;
    }

    public void setSchedule(WeekDays oneDay, Tasks oneTask) {
        this.schedule.setOneTask(oneDay, oneTask);
    }

    public Schedule getSchedule() {
        return schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        if (this == o) return true;
        Human human = (Human) o;
        if (this.hashCode() != human.hashCode()) return false;
        return Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(year, human.year) &&
                Objects.equals(iq, human.iq) &&
                Objects.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq, schedule);
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Об'єкт Human буде видалений: " + this.toString());
        } finally {
            super.finalize();
        }
    }

}
