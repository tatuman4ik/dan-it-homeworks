public class Schedule {
    private final int days = WeekDays.values().length;
    private String[][] schedule = new String[days][2];
    private WeekDays[] week = WeekDays.values();
    private Tasks[] tasks = Tasks.values();

    public Schedule() {
        int i = 0;
        for (WeekDays oneDay : week) {
            schedule[i][0] = oneDay.name();
            i++;
        }
    }

    public void setOneTask(WeekDays oneDay, Tasks oneTask) {
        this.schedule[oneDay.ordinal()][1] = oneTask.toString();
    }

    @Override
    public String toString() {
        String taskList = "";
        for (int dayNum = 0; dayNum < days; dayNum++) {
            if (schedule[dayNum][1] != null) {
                if (!taskList.isEmpty()) taskList += ", ";
                taskList += "[" + schedule[dayNum][0] + ", " + schedule[dayNum][1] + "]";
            }
        }
        return taskList;
    }
}
