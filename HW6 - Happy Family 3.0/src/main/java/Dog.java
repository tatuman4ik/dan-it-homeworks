public class Dog extends Pet implements CanFoul{

    private boolean innocent = true;

    public Dog(String nickname, int age, int trickLevel, Habits[] habits, boolean hungry) {
        super(nickname, age, trickLevel, habits, hungry);
        setSpecies(Species.DOG);
    }

    public Dog(String nickname, int age, Habits[] habits) {
        super(nickname, age, 0, habits, false);
        setSpecies(Species.DOG);
    }

    public Dog(String nickname) {
        super(nickname, 0, 0, new Habits[]{}, false);
        setSpecies(Species.DOG);
    }

    public Dog() {
        super(null, 0, 0, new Habits[]{}, false);
        setSpecies(Species.DOG);
    }

    public void eat() {
        Screen.print("Гав-гав! Смачно поїв!");
        setHungry(false);
    }

    public void foul() {
        Screen.print("Треба добре замісти сліди...");
        int level = getTrickLevel();
        if (isLucky()) {
            setInnocent(true);
            if (level < 100) level++;
        } else {
            setInnocent(false);
            if (level > 0) level--;
        }
        setTrickLevel(level);
    }

    public void setInnocent(boolean innocent) {
        this.innocent = innocent;
    }

    public boolean isInnocent() {
        return innocent;
    }

}
