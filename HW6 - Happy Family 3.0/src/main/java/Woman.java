import java.time.Year;

public final class Woman extends Human {

    public Woman(String name, String surname, Year year, int iq, Schedule schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman(String name, String surname, Year year) {
        super(name, surname, year, 0, new Schedule());
    }

    public Woman(String name, String surname, int iq, Family family) {
        super(name, surname, Year.now(), iq, null);
        setFamily(family);
    }

    public Woman() {
        super(null, null, null);
    }

    @Override
    public void greetPet() {
        if (getFamily() != null && getFamily().getPet() != null) {
            String message = "Привіт, " + getFamily().getPet().getNickname() + ", усі-пусі, йди до мене!";
            Screen.print(message);
            getFamily().getPet().respond(this);
        }
    }

    public void makeUp() {
        Screen.print(getName() + " фарбується біля дзеркала");
    }
}
