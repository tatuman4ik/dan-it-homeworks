import java.time.Year;

public final class Man extends Human {

    public Man(String name, String surname, Year year, int iq, Schedule schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, Year year) {
        super(name, surname, year, 0, new Schedule());
    }

    public Man(String name, String surname, int iq, Family family) {
        super(name, surname, Year.now(), iq, null);
        setFamily(family);
    }

    public Man() {
        super(null, null, null);
    }
    
    @Override
    public void greetPet() {
        if (getFamily() != null && getFamily().getPet() != null) {
            String message = "Привіт, " + getFamily().getPet().getNickname() + ", давай гратися!";
            Screen.print(message);
            getFamily().getPet().respond(this);
        }
    }

    public void repairCar() {
        Screen.print(getName() + " пішов до сервісу");
    }
}
