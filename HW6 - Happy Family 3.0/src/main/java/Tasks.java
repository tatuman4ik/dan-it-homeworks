public enum Tasks {
    WORK,
    MEETING,
    GYM,
    CINEMA,
    CLEANING,
    LAUNDRY,
    PICNIC,
    STUDY,
    DATE,
    NOTHING
}
