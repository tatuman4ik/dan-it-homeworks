public enum Species {
    DOG(false, 4, true),
    CAT(false, 4, true),
    ROBOCAT(false, 4, false),
    BIRD(true, 2, false),
    FISH(false, 0, false),
    RABBIT(false, 4, true),
    TURTLE(false, 4, false),
    SNAKE(false, 0, false),
    UNKNOWN(false, 0, false);


    private final boolean canFly;
    private final int numberOfLegs;
    private final boolean hasFur;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    public boolean canFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean hasFur() {
        return hasFur;
    }

    public static Species getSpecies(String speciesName) {
        try {
            return Species.valueOf(speciesName.toUpperCase());
        } catch (IllegalArgumentException e) {
            return Species.UNKNOWN;
        }
    }
}
