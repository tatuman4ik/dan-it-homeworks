import java.util.Objects;
import java.util.Random;

public abstract class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Habits[] habits;
    private boolean hungry;
    private Family family;

    public Pet(String species, String nickname, int age, int trickLevel, Habits[] habits, boolean hungry) {
        this.species = Species.getSpecies(species);
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.hungry = hungry;
    }

    public Pet(String nickname, int age, int trickLevel, Habits[] habits, boolean hungry) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.hungry = hungry;
    }

    public Pet(String nickname, int age, Habits[] habits) {
        this(nickname, age, 0, habits, false);
    }

    public Pet(String nickname) {
        this(nickname, 0, 0, new Habits[]{}, false);
    }

    public Pet() {}

    public void eat() {}

    public void respond(Human human) {
        Screen.print("Привіт, хазяїн " + human.getName() + "! Я " + getNickname() + ". Я скучив!");
    }

    public boolean isLucky() {
        Random random = new Random();
        if (getTrickLevel() > random.nextInt(101))
            return true;
        return false;
    }

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder();
        if (species != null) message.append(species);
        message.append("{");
        if (nickname != null) message.append("nickname='").append(nickname).append("'");
        message.append(", age=").append(age).append(", trickLevel=").append(trickLevel);
        if (species != null) {
            message.append(", canFly=").append(species.canFly());
            message.append(", numberOfLegs=").append(species.getNumberOfLegs());
            message.append(", hasFur=").append(species.hasFur());
        }
        if (habits != null && habits.length > 0) {
            message.append(", habits=[");
            for (int i = 0; i < habits.length; i++) {
                if (i > 0) message.append(", ");
                message.append(habits[i].name());
            }
            message.append("]");
        }
        message.append("}");
        return message.toString();
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setSpecies(Species species) {
        this.species = species != null ? species : Species.UNKNOWN;
    }

    public void setSpecies(String species) {
        this.species = Species.getSpecies(species);
    }

    public Species getSpecies() {
        return species;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setHabits(Habits[] habits) {
        this.habits = habits;
    }

    public Habits[] getHabits() {
        return habits;
    }

    public void setHungry(boolean hungry) {
        this.hungry = hungry;
    }

    public boolean isHungry() {
        return hungry;
    }

    public void setFamily(Family family) {
        setFamily(family, false);
    }

    public void setFamily(Family family, boolean isFreePet) {
        if (family != null) {
            if (!isFreePet) {
                if (this.hasFamily()) this.family.deletePet();
                this.family = null;
                family.setPet(this, true);
            }
        }
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public boolean hasFamily() {
        return this.family != null;
    }

    public void deleteFamily() {
        this.family = null;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        if (this == o) return true;
        Pet pet = (Pet) o;
        if (hashCode() != pet.hashCode()) return false;
        return age == pet.age &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname) &&
                Objects.equals(trickLevel, pet.trickLevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Об'єкт Pet буде видалений: " + this);
        } finally {
            super.finalize();
        }
    }
}
