public class RoboCat extends Pet{

    public RoboCat(String nickname, int age, int trickLevel, Habits[] habits, boolean hungry) {
        super(nickname, age, trickLevel, habits, hungry);
        setSpecies(Species.ROBOCAT);
    }
    public RoboCat(String nickname, int age, Habits[] habits) {
        super(nickname, age, 0, habits, false);
        setSpecies(Species.ROBOCAT);
    }

    public RoboCat(Species species, String nickname) {
        super(nickname, 0, 0, new Habits[]{}, false);
        setSpecies(Species.ROBOCAT);
    }

    public RoboCat() {
        super(null, 0, 0, new Habits[]{}, false);
        setSpecies(Species.ROBOCAT);
    }

    public void eat() {
        Screen.print("Швидка зарядка!");
        setHungry(false);
    }
}
