import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Family implements HumanCreator {
    private final Human parent1;
    private final Human parent2;
    private Human[] children;
    private Pet pet;

    public Family() {
        throw new IllegalArgumentException("Мають бути двоє людей. Працює пустий конструктор");
    }

    public Family(Human parent1, Human parent2) {
        if (parent1 == null || parent2 == null ||
                parent1.equals(new Human()) || parent2.equals(new Human()) ||
                parent1.equals(parent2))
            throw new IllegalArgumentException("Мають бути двоє людей");
        if (parent1.getFamily() != null && parent2.getFamily() != null &&
                parent1.getFamily().equals(parent2.getFamily()))
            throw new IllegalArgumentException("Ці двоє вже й так в одній сім'ї");
        this.parent1 = parent1;
        this.parent2 = parent2;
        this.parent1.setFamily(this);
        this.parent2.setFamily(this);
        this.children = new Human[0];
    }

    public Human getParent1() {
        return parent1;
    }

    public Human getParent2() {
        return parent2;
    }

    public void setChildren(Human[] children) {
        for (Human child : children) {
            addChild(child);
        }
    }

    public Human bornChild() {
        Random random = new Random();
        String surname = random.nextBoolean() ? parent1.getSurname() : parent2.getSurname();
        int iq = (parent1.getIq() + parent2.getIq()) / 2;
        String[] maleNames = { "Володимир", "Любомир", "Святослав", "Григорій", "Євген" };
        String[] femaleNames = { "Марія", "Анна", "Ірина", "Ольга", "Катерина" };
        return random.nextBoolean() ? new Man(maleNames[random.nextInt(maleNames.length)], surname, iq, this)
                                    : new Woman(femaleNames[random.nextInt(femaleNames.length)], surname, iq, this);
    }

    public void addChild(Human newChild) {
        if (!this.equals(newChild.getFamily())) newChild.setFamily(this);
        if (!this.hasThisChild(newChild) && !this.parent1.equals(newChild) &&
                !this.parent2.equals(newChild)) {
            Human[] newChildArray = new Human[this.children.length + 1];
            System.arraycopy(this.children, 0, newChildArray, 0, this.children.length);
            newChildArray[this.children.length] = newChild;
            this.children = newChildArray;
        }
    }

    public boolean hasThisChild(Human child) {
        for (Human kid: this.children) {
            if (kid.equals(child)) return true;
        }
        return false;
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= this.children.length) {
            return false;
        }
        Human toDelete = children[index];
        Human[] newChildArray = new Human[this.children.length - 1];
        int newIndex = 0;
        for (int i = 0; i < this.children.length; i++) {
            if (i != index) {
                newChildArray[newIndex++] = this.children[i];
            }
        }
        this.children = newChildArray;
        toDelete.setFamily(null);
        return true;
    }

    public boolean deleteChild(Human child) {
        boolean isDeleted = false;
        for (int i = 0; i < children.length; i++)
            if (children[i].equals(child)) isDeleted = deleteChild(i);
        return isDeleted;
    }

    public int countFamily() { return children.length + 2; }

    public Human[] getChildren() {
        return children;
    }

    public void setPet(Pet pet) {
        setPet(pet, false);
    }

    public void setPet(Pet pet, boolean isFreePet) {
        if (this.hasPet() && !this.pet.equals(pet)) this.pet.deleteFamily();
        if (pet != null) {
            pet.setFamily(this, isFreePet);
        }
        this.pet = pet;
    }

    public boolean hasPet() {
        return this.pet != null;
    }

    public void deletePet() {
        this.pet = null;
    }

    public Pet getPet() {
        return pet;
    }

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder(getClass().getSimpleName() + "{батьки:'" + getParent1().getHumanFieldsInfo() +
                "', '" + getParent2().getHumanFieldsInfo() + "'");
        if (children.length > 0) {
            message.append(", children:");
            for (int child = 0; child < children.length; child++) {
                if (child > 0) message.append(", ");
                message.append("'").append(children[child].getHumanFieldsInfo()).append("'");
            }
        }
        if (pet != null) message.append(", pet:'").append(getPet().toString()).append("'");
        message.append("}");
        return message.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        if (this == o) return true;
        Family family = (Family) o;
        if (hashCode() != family.hashCode()) return false;
        return Objects.equals(parent1, family.parent1) &&
                Objects.equals(parent2, family.parent2);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(parent1, parent2, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Об'єкт Family буде видалений: " + this);
        } finally {
            super.finalize();
        }
    }
}