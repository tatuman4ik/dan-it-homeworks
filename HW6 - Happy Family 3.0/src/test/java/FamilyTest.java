import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Year;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    private Human NotNullHuman1;
    private Human NotNullHuman2;
    private Human EmptyFieldHuman1;
    private Human EmptyFieldHuman2;
    private Human NullHuman1;
    private Human NullHuman2;
    private DomesticCat Cat;
    private Dog Dog;
    private Pet EmptyPet;
    private Pet NullPet;

    @BeforeEach
    public void setup() {
        NotNullHuman1 = new Human("John", "Stevenson", Year.of(2000));
        NotNullHuman2 = new Human("Mary", "Johnson", Year.of(2003));
        EmptyFieldHuman1 = new Human();
        EmptyFieldHuman2 = new Human();
        NullHuman1 = null;
        NullHuman2 = null;
        Cat = new DomesticCat("Cat");
        Dog = new Dog("Dog");
        EmptyPet = new Fish();
        NullPet = null;
    }

    @Test
    void constructorWithoutArgumentsShouldThrowException() {
        assertThrows(
                IllegalArgumentException.class,
                Family::new
        );
    }

    @Test
    void constructorWith2NullArgumentsShouldThrowException() {
        assertNull(NullHuman1);
        assertNull(NullHuman2);

        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NullHuman1, NullHuman2)
        );
    }

    @Test
    void constructorWithNullAndNotNullArgumentsShouldThrowException() {
        assertNull(NullHuman1);
        assertNotNull(NotNullHuman1);
        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NullHuman1, NotNullHuman1)
        );

        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NotNullHuman1, NullHuman1)
        );
    }

    @Test
    void constructorWith2EqualArgumentsShouldThrowException() {
        assertEquals(
                NotNullHuman1,
                NotNullHuman1
        );
        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NotNullHuman1, NotNullHuman1)
        );
    }

    @Test
    void constructorWith2DifferentArgumentsShouldExecute() {
        assertNotEquals(
                NotNullHuman1,
                NotNullHuman2
        );
        assertDoesNotThrow(
                () -> new Family(NotNullHuman1, NotNullHuman2)
        );
    }

    @Test
    void constructorWith2NotNullDifferentArgumentsButEqualFamilyShouldThrowException() {
        assertNotEquals(
                NotNullHuman1,
                NotNullHuman2
        );
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                family,
                NotNullHuman1.getFamily()
        );
        assertEquals(
                family,
                NotNullHuman2.getFamily()
        );

        assertThrows(
                IllegalArgumentException.class,
                () -> new Family(NotNullHuman1, NotNullHuman2)
        );
    }

    @Test
    void getParent1ShouldReturnFirstParent() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                NotNullHuman1.getFamily(),
                NotNullHuman2.getFamily()
        );

        assertEquals(
                NotNullHuman1,
                family.getParent1()
        );

        assertNotEquals(
                NotNullHuman2,
                family.getParent1()
        );
    }

    @Test
    void getParent2ShouldReturnSecondParent() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                NotNullHuman1.getFamily(),
                NotNullHuman2.getFamily()
        );

        assertEquals(
                NotNullHuman2,
                family.getParent2()
        );

        assertNotEquals(
                NotNullHuman1,
                family.getParent2()
        );
    }

    @Test
    void setChildrenShouldSetChildrenArrayToFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        Human[] children = new Human[]{EmptyFieldHuman1, EmptyFieldHuman2};

        assertNotEquals(
                family,
                EmptyFieldHuman1.getFamily()
        );
        assertNotEquals(
                family,
                EmptyFieldHuman2.getFamily()
        );

        int beforeLength = family.getChildren().length;

        assertDoesNotThrow(
                () -> family.setChildren(children)
        );

        int afterLength = family.getChildren().length;

        assertEquals(
                beforeLength,
                afterLength - children.length
        );
        assertEquals(
                family,
                EmptyFieldHuman1.getFamily()
        );
        assertEquals(
                family,
                EmptyFieldHuman2.getFamily()
        );
    }

    @Test
    void addChildShouldSetChildToFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertNotEquals(
                family,
                EmptyFieldHuman1.getFamily()
        );

        int beforeLength = family.getChildren().length;

        assertDoesNotThrow(
                () -> family.addChild(EmptyFieldHuman1)
        );

        int afterLength = family.getChildren().length;

        assertEquals(
                beforeLength,
                afterLength - 1
        );
        assertEquals(
                family,
                EmptyFieldHuman1.getFamily()
        );
        assertTrue(
                family.hasThisChild(EmptyFieldHuman1)
        );
    }

    @Test
    void addChildShouldSetChildToFamilyIfIsNotParent1() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                family.getParent1(),
                NotNullHuman1
        );
        int beforeLength = family.getChildren().length;

        assertDoesNotThrow(
                () -> family.addChild(NotNullHuman1)
        );
        int afterLength = family.getChildren().length;

        assertEquals(
                beforeLength,
                afterLength
        );
        assertFalse(
                family.hasThisChild(NotNullHuman1)
        );
    }

    @Test
    void addChildShouldSetChildToFamilyIfIsNotParent2() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                family.getParent2(),
                NotNullHuman2
        );
        int beforeLength = family.getChildren().length;

        assertDoesNotThrow(
                () -> family.addChild(NotNullHuman2)
        );
        int afterLength = family.getChildren().length;

        assertEquals(
                beforeLength,
                afterLength
        );
        assertFalse(
                family.hasThisChild(NotNullHuman2)
        );
    }

    @Test
    void addChildNotAddTwiceIfAlreadyHasThisChild() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        assertTrue(
                family.hasThisChild(EmptyFieldHuman1)
        );

        int beforeLength = family.getChildren().length;

        assertDoesNotThrow(
                () -> family.addChild(EmptyFieldHuman1)
        );

        int afterLength = family.getChildren().length;

        assertTrue(
                family.hasThisChild(EmptyFieldHuman1)
        );
        assertEquals(
                beforeLength,
                afterLength
        );
    }

    @Test
    void hasThisChildShouldReturnTrueIfChildAlreadyChildThisFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        assertEquals(
                family,
                EmptyFieldHuman1.getFamily()
        );
        assertTrue(
                family.hasThisChild(EmptyFieldHuman1)
        );
    }

    @Test
    void hasThisChildShouldReturnFalseIfChildNotThisFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertNotEquals(
                family,
                EmptyFieldHuman1.getFamily()
        );
        assertFalse(
                family.hasThisChild(EmptyFieldHuman1)
        );
    }

    @Test
    void hasThisChildShouldReturnFalseIfChildIsParent() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                family.getParent1(),
                NotNullHuman1
        );
        assertFalse(
                family.hasThisChild(NotNullHuman1)
        );

        assertEquals(
                family.getParent2(),
                NotNullHuman2
        );
        assertFalse(
                family.hasThisChild(NotNullHuman2)
        );
    }

    @Test
    void deleteChildObjectFamilyTrue() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        assertTrue(
                family.hasThisChild(EmptyFieldHuman1)
        );

        int beforeLength = family.getChildren().length;

        assertTrue(
                family.deleteChild(EmptyFieldHuman1)
        );

        int afterLength = family.getChildren().length;

        assertFalse(
                family.hasThisChild(EmptyFieldHuman1)
        );
        assertEquals(
                beforeLength - 1,
                afterLength
        );
    }

    @Test
    void deleteChildObjectFamilyFalse() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        assertFalse(
                family.hasThisChild(EmptyFieldHuman2)
        );

        int beforeLength = family.getChildren().length;

        assertFalse(
                family.deleteChild(EmptyFieldHuman2)
        );

        int afterLength = family.getChildren().length;

        assertFalse(
                family.hasThisChild(EmptyFieldHuman2)
        );
        assertEquals(
                beforeLength,
                afterLength
        );

    }

    @Test
    void deleteChildIndexFromChildrenArrayFamilyTrue() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        assertTrue(
                family.hasThisChild(EmptyFieldHuman1)
        );

        int beforeLength = family.getChildren().length;

        assertTrue(
                family.deleteChild(0)
        );

        int afterLength = family.getChildren().length;

        assertFalse(
                family.hasThisChild(EmptyFieldHuman1)
        );
        assertEquals(
                beforeLength - 1,
                afterLength
        );
    }

    @Test
    void deleteChildIndexFromChildrenArrayFamilyFalse() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        family.addChild(EmptyFieldHuman1);

        int beforeLength = family.getChildren().length;

        assertFalse(
                family.deleteChild(1)
        );

        int afterLength = family.getChildren().length;

        assertEquals(
                beforeLength,
                afterLength
        );

    }

    @Test
    void countFamilyEqual2ParentsWithoutAnyChild() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                2,
                family.countFamily()
        );

        assertThrows(
                NullPointerException.class,
                () -> family.addChild(NullHuman1)
        );
        assertEquals(
                2,
                family.countFamily()
        );
    }

    @Test
    void countFamilyEqual2ParentsAndChildrenArrayLength() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);
        Human[] children = new Human[]{EmptyFieldHuman1, EmptyFieldHuman2};

        int beforeLength = family.countFamily();

        assertDoesNotThrow(
                () -> family.setChildren(children)
        );
        assertEquals(
                beforeLength + children.length,
                family.countFamily()
        );

        beforeLength = family.countFamily();

        assertDoesNotThrow(
                () -> family.deleteChild(EmptyFieldHuman1)
        );

        assertEquals(
                beforeLength - 1,
                family.countFamily()
        );
    }

    @Test
    void getChildrenEqualNullWhenNoChildInFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertEquals(
                0,
                family.getChildren().length
        );

        assertArrayEquals(
                new Human[0],
                family.getChildren()
        );
    }

    @Test
    void setPetItHasNoFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertNull(
                Cat.getFamily()
        );

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertEquals(
                Cat,
                family.getPet()
        );
    }

    @Test
    void setPetItAlreadyHasFamily() {
        Family family1 = new Family(NotNullHuman1, NotNullHuman2);
        Family family2 = new Family(EmptyFieldHuman1, EmptyFieldHuman2);

        assertDoesNotThrow(
                () -> family1.setPet(Cat)
        );

        assertEquals(
                Cat,
                family1.getPet()
        );

        assertNotEquals(
                Cat,
                family2.getPet()
        );

        assertDoesNotThrow(
                () -> family2.setPet(Cat)
        );

        assertEquals(
                Cat,
                family2.getPet()
        );

        assertNotEquals(
                Cat,
                family1.getPet()
        );
    }

    @Test
    void setPetFamilyAlreadyHasOtherPet() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertEquals(
                Cat,
                family.getPet()
        );

        assertNotEquals(
                Dog,
                family.getPet()
        );

        assertDoesNotThrow(
                () -> family.setPet(Dog)
        );

        assertEquals(
                Dog,
                family.getPet()
        );

        assertNotEquals(
                Cat,
                family.getPet()
        );
    }

    @Test
    void setPetNullWhenPetNull() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertNotNull(
                family.getPet()
        );

        assertNull(
                NullPet
        );

        assertDoesNotThrow(
                () -> family.setPet(NullPet)
        );

        assertNull(
                family.getPet()
        );
    }

    @Test
    void setPetItHasNoFamilyBooleanTrue() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertNull(
                Cat.getFamily()
        );

        assertDoesNotThrow(
                () -> family.setPet(Cat, true)
        );

        assertEquals(
                Cat,
                family.getPet()
        );
    }

    @Test
    void setPetItAlreadyHasFamilyBooleanTrue() {
        Family family1 = new Family(NotNullHuman1, NotNullHuman2);
        Family family2 = new Family(EmptyFieldHuman1, EmptyFieldHuman2);

        assertDoesNotThrow(
                () -> family1.setPet(Cat, true)
        );

        assertEquals(
                Cat,
                family1.getPet()
        );

        assertNotEquals(
                Cat,
                family2.getPet()
        );

        assertDoesNotThrow(
                () -> family2.setPet(Cat, true)
        );

        assertEquals(
                Cat,
                family2.getPet()
        );

        assertEquals(
                Cat,
                family1.getPet()
        );
    }

    @Test
    void setPetFamilyAlreadyHasOtherPetBooleanTrue() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat, true)
        );

        assertEquals(
                Cat,
                family.getPet()
        );

        assertNotEquals(
                Dog,
                family.getPet()
        );

        assertDoesNotThrow(
                () -> family.setPet(Dog, true)
        );

        assertEquals(
                Dog,
                family.getPet()
        );

        assertNotEquals(
                Cat,
                family.getPet()
        );
    }

    @Test
    void setPetNullWhenPetNullBooleanTrue() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat, true)
        );

        assertNotNull(
                family.getPet()
        );

        assertNull(
                NullPet
        );

        assertDoesNotThrow(
                () -> family.setPet(NullPet, true)
        );

        assertNull(
                family.getPet()
        );
    }

    @Test
    void setPetItHasNoFamilyBooleanFalse() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertNull(
                Cat.getFamily()
        );

        assertDoesNotThrow(
                () -> family.setPet(Cat, false)
        );

        assertEquals(
                Cat,
                family.getPet()
        );
    }

    @Test
    void setPetItAlreadyHasFamilyBooleanFalse() {
        Family family1 = new Family(NotNullHuman1, NotNullHuman2);
        Family family2 = new Family(EmptyFieldHuman1, EmptyFieldHuman2);

        assertDoesNotThrow(
                () -> family1.setPet(Cat, false)
        );

        assertEquals(
                Cat,
                family1.getPet()
        );

        assertNotEquals(
                Cat,
                family2.getPet()
        );

        assertDoesNotThrow(
                () -> family2.setPet(Cat, false)
        );

        assertEquals(
                Cat,
                family2.getPet()
        );

        assertNotEquals(
                Cat,
                family1.getPet()
        );
    }

    @Test
    void setPetFamilyAlreadyHasOtherPetBooleanFalse() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat, false)
        );

        assertEquals(
                Cat,
                family.getPet()
        );

        assertNotEquals(
                Dog,
                family.getPet()
        );

        assertDoesNotThrow(
                () -> family.setPet(Dog, false)
        );

        assertEquals(
                Dog,
                family.getPet()
        );

        assertNotEquals(
                Cat,
                family.getPet()
        );
    }

    @Test
    void setPetNullWhenPetNullBooleanFalse() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat, false)
        );

        assertNotNull(
                family.getPet()
        );

        assertNull(
                NullPet
        );

        assertDoesNotThrow(
                () -> family.setPet(NullPet, false)
        );

        assertNull(
                family.getPet()
        );
    }

    @Test
    void hasPetShouldTrueFamilyHasPet() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertNotNull(
                family.getPet()
        );

        assertTrue(
                family.hasPet()
        );
    }

    @Test
    void hasPetShouldFalseFamilyHasNotPet() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(NullPet)
        );

        assertNull(
                family.getPet()
        );

        assertFalse(
                family.hasPet()
        );
    }

    @Test
    void deletePetMakeNullPetToFamily() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertNotNull(
                family.getPet()
        );

        assertTrue(
                family.hasPet()
        );

        assertDoesNotThrow(
                family::deletePet
        );

        assertNull(
                family.getPet()
        );

        assertFalse(
                family.hasPet()
        );
    }

    @Test
    void getPetShouldReturnPetFamilyHasPet() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(Cat)
        );

        assertEquals(
                Cat,
                family.getPet()
        );
    }

    @Test
    void getPetShouldReturnNullFamilyHasNoPet() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        assertDoesNotThrow(
                () -> family.setPet(NullPet)
        );

        assertNull(
                family.getPet()
        );
    }
    @Test
    void toStringShouldBeEqualToExample() {
        Family family = new Family(NotNullHuman1, NotNullHuman2);

        String example;
        example = "Family{батьки:'Human{name='John', surname='Stevenson', year=2000}', 'Human{name='Mary', surname='Johnson', year=2003}'}";

        assertEquals(
                example,
                family.toString()
        );
    }
    @Test
    void toStringShouldEmptyWhenEmpty() {
        Family family = new Family(EmptyFieldHuman1, EmptyFieldHuman2);
        String example;
        example = "Family{батьки:'Human{}', 'Human{}'}";

        assertEquals(
                example,
                family.toString()
        );
    }
}