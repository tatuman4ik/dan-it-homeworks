public class Questionary {
    private static String[][] years = {
            {"2022", "Коли відбулося повномасштабне вторгнення орків на нашу неньку?"},
            {"1991", "Коли Україна отримала незалежність?"},
            {"1812", "Коли москва палала як щепка?"}
    };
    public static int questCount() {
        return years.length;
    }
    public static String[] event(int i) {
        return years[i];
    }
}
