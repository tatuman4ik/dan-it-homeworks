import java.util.Random;

public class Method {
    public static int randomNumber(int till) {
        Random rand = new Random();
        return rand.nextInt(till);
    }
    public static boolean numberExist(int[] arr, int length, int temp){
        for (int i = 0; i <= length; i++) {
            if (temp == arr[i]) {
                Screen.error("Ти вже вводив це число");
                return true;
            }
        }
        return false;
    }
    public static void sort(int[] arr, int length) {
        for (int gap = length/2; gap > 0; gap /= 2) {
            for (int i = gap; i < length; i++) {
                int temp = arr[i];
                int j;
                for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                    arr[j] = arr[j - gap];
                }
                arr[j] = temp;
            }
        }
        Screen.print("Ти вже вводив такі числа:");
        Screen.printArray(arr, length);
    }
    public static boolean compare(int random, int number) {
        if (random == number) return true;
        String tryAgain = (random < number) ?
                "Число " + Integer.toString(number) + " завелике" :
                "Число " + Integer.toString(number) + " замале";
        Screen.print(tryAgain);
        return false;
    }
}
