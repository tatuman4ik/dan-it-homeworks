import java.util.Scanner;

public class Screen {

    static Scanner scan = new Scanner(System.in);

    public static String read() {
        return scan.nextLine();
    }

    public static void print(String line) {
        System.out.printf("%s\n", line);
    }
    public static void error(String line) {
        System.err.printf("%s\n", line);
    }

    public static void printScore(String line, String game, String score) {
        System.out.printf(line.toString(), game, score);
    }
    public static void printArray(int[] array, int lenght) {
        int i = 0;
        while (i < lenght) {
            System.out.printf("%" + 5 + "d", array[i]);
            i++;
        }
        System.out.println();
    }

    public static int enterNumber(int from, int till) {
        do {
            try {
                int number = Integer.parseInt(read());
                if (number >= from && number <= till)
                    return number;
                else
                    error ("Такого варіанту немає");
            } catch (NumberFormatException exception) {
                error ("Тільки число!");
            }
        }while (true);
    }
}