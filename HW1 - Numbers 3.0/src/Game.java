public class Game {
    static Score totalScore = new Score();
    public static void main(String[] args) {
        BeginMenu currentGame = new BeginMenu();
        final String user = currentGame.enterName();
        do {
            currentGame.menu();
            Screen.print("Гра починається!");
            String gameName;
            if (currentGame.gamePointer == 1)
                gameName = gameRandom(user);
            else
                gameName = gameHistory(user);
            totalScore.printScore(user, gameName);
        } while (true);
    }

    public static String gameRandom(String user) {

        String gameName = "Рандомне число";
        final int till = 100;
        int tryNum = 10;
        int random = Method.randomNumber(till + 1);
        Screen.print("Відгадай цифру від 0 до 100 за " + tryNum + " спроб");

        gameStart(tryNum, till, random, user, gameName);

        return gameName;
    }

    private static String gameHistory(String user) {

        String gameName = "Події в історії";
        final int till = 2023;
        int tryNum = 10;
        int randInt = Method.randomNumber(Questionary.questCount());
        int random = Integer.parseInt(Questionary.event(randInt)[0]);
        String question = Questionary.event(randInt)[1];
        Screen.print("Відгадай рік за " + tryNum + " спроб\n" + question);

        gameStart(tryNum, till, random, user, gameName);

        return gameName;
    }
    private static void gameStart(int tryNum,
                                  int till,
                                  int random,
                                  String user,
                                  String gameName) {

        int[] number = new int[tryNum];
        int score = tryNum * 11;
        int i = 0;
        do {
            Screen.print((i + 1) + " спроба");
            int temp = Screen.enterNumber(0, till);
            if (!Method.numberExist(number, i - 1, temp)) {
                number[i] = temp;
                i++;
            }
            score -= tryNum;
        } while (!Method.compare(random, number[i - 1]) && i < 10);
        if (Method.compare(random, number[i - 1])) {
            Screen.print("Вітаю з перемогою, " + user + "!\n");
            Method.sort(number, i - 1);
            Screen.print("Твої бали: " + score);
            totalScore.addScore(gameName, score);
        } else
            Screen.print("Ти програв та не заробив балів :(\nВідповідь: " + random);
    }




}
