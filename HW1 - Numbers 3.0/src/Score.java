public class Score {
    private String[][] score = new String[100][2];
    private int currentIndex = 0;

    public void addScore(String gameName, int score) {
        try {
            this.score[currentIndex][0] = gameName;
            this.score[currentIndex][1] = Integer.toString(score);
            currentIndex++;
        } catch (Exception E) {
            Screen.error("На жаль, таблиця вже повна");
        }
    }

    public String[][] getScore() {
        return score;
    }
    private void sortScore() {
        for (int gap = currentIndex/2; gap > 0; gap /= 2) {
            for (int i = gap; i < currentIndex; i++) {
                int tempInt = Integer.parseInt(score[i][1]);
                String tempStr = score[i][0];
                int j;
                for (j = i; j >= gap && Integer.parseInt(score[j - gap][1]) < tempInt; j -= gap) {
                    score[j][0] = score[j - gap][0];
                    score[j][1] = score[j - gap][1];
                }
                score[j][0] = tempStr;
                score[j][1] = String.valueOf(tempInt);
            }
        }
    }
    public void printScore(String user, String gameName) {
        String[][] score = this.getScore();
        Screen.print(user + "\nОсь твоя рейтингова таблиця:");
        Screen.print("_".repeat(27));
        sortScore();
        for (int i = 0; (i < score.length); i++) {
            if (score[i][0].equals(gameName)) {
                Screen.printScore("| %-16s | %-4s |%n", score[i][0], score[i][1]);
                Screen.print("_".repeat(30));
            }
        }
    }
}