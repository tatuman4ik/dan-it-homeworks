import java.util.Random;

public class Numbers {
    final static int till = 101;
    public static void main(String[] args) {
        String name = enterName();
        int random = randomNumber(till);
        startGame(name, random);
    }
    public static void output(String line) {
        System.out.printf("%s\n", line);
    }
    public static void error(String line) {
        System.err.printf("%s\n", line);
    }
    public static String enterName() {
        output("Enter your name please");
        Input input = new Input();
        return (input.readLine());
    }
    public static int randomNumber(int till) {
        Random rand = new Random();
        return rand.nextInt(till);
    }
    public static void startGame(String name, int random) {
        output("Let the game begin!");
        int[] number;
        number = new int[till];
        int i = 0;
        do {
            int temp = enter();
            if (!numberExist(number, i, temp)) {
                number[i] = temp;
                i++;
            }
        } while (!compare(random, number[i - 1]));
        Numbers.sort(number, i);
        output("Congratulations, " + name + "!\n" + "You've entered:");
        int j = 0;
        while (j < i) {
            System.out.println(number[j]);
            j++;
        }
    }
    public static boolean numberExist(int[] arr, int length, int temp){
        for (int i = 0; i < length; i++) {
            if (temp == arr[i]) {
                error("You had checked number " + temp + " before");
                return true;
            }
        }
        return false;
    }
    public static void sort(int[] arr, int length) {
        for (int gap = length/2; gap > 0; gap /= 2) {
            for (int i = gap; i < length; i++) {
                int temp = arr[i];
                int j;
                for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                    arr[j] = arr[j - gap];
                }
                arr[j] = temp;
            }
        }
    }
    public static int enter() {
        do {
            output("Enter your number from 0 to " + (till - 1));
            Input input = new Input();
            String answer = input.readLine();
            try {
                int number = Integer.parseInt(answer);
                if (number >= 0 && number <= till - 1)
                    return number;
                else
                    error ("Only number from 0 to " + (till - 1) + " please");
            } catch (NumberFormatException exception) {
                error ("Only number from 0 to " + (till - 1) + " please");
            }
        }while (true);
    }
    public static boolean compare(int random, int number) {
        if (random == number) return true;
        String tryAgain = (random < number) ?
                "Number " + Integer.toString(number) + " is too big" :
                "Number " + Integer.toString(number) + " is too small";
        output(tryAgain);
        return false;
    }
}
