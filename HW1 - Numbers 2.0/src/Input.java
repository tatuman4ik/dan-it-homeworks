import java.util.Scanner;

public class Input {
    private Scanner scan = new Scanner(System.in);
    public String readLine() {
        return scan.nextLine();
    }
}