package app.controller;

import app.models.Account;
import app.models.Customer;
import app.service.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/{customerId}")
    public Customer getCustomer(@PathVariable Long customerId) {
        return customerService.getCustomerWithAccounts(customerId);
    }

    @GetMapping
    public List<Customer> getAllCustomers() {
        return customerService.getAllCustomersWithAccounts();
    }

    @PostMapping
    public Customer createCustomer(@RequestBody Customer customer) {
        return customerService.createCustomer(customer.getName(), customer.getEmail(), customer.getAge());
    }

    @PutMapping("/{customerId}")
    public Customer updateCustomer(@PathVariable Long customerId, @RequestBody Customer customer) {
        return customerService.updateCustomer(customerId, customer.getName(), customer.getEmail(), customer.getAge());
    }

    @DeleteMapping("/{customerId}")
    public void deleteCustomer(@PathVariable Long customerId) {
        customerService.deleteCustomer(customerId);
    }

    @PostMapping("/{customerId}/accounts")
    public Account createAccount(@PathVariable Long customerId, @RequestBody Account account) {
        return customerService.createAccount(customerId, account.getCurrency());
    }

    @DeleteMapping("/{customerId}/accounts/{accountId}")
    public void deleteAccount(@PathVariable Long customerId, @PathVariable Long accountId) {
        customerService.deleteAccount(customerId, accountId);
    }
}
