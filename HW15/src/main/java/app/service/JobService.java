package app.service;

import app.dao.Dao;
import app.models.Customer;
import app.models.Employer;
import app.models.Job;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class JobService {

    private final Dao<Job> jobDao;

    public Job create(Customer customer, Employer employer) {
        return (customer != null && employer != null)
                ? jobDao.save(new Job(customer, employer))
                : null;
    }

    public void delete(long jobId) {
        Job existingjob = jobDao.getOne(jobId);
        if (existingjob != null) {
            jobDao.delete(existingjob);
        }
    }
}
