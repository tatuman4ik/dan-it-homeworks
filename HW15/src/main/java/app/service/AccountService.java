package app.service;

import app.dao.Dao;
import app.models.Account;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class AccountService {

    private final Dao<Account> accountDao;

    private Optional<Account> findByNumber(String accountNumber) {
        return Optional.of(accountDao.getOne(Long.parseLong(accountNumber)));
    }

    public void deposit(String accountNumber, double amount) {
        Optional<Account> account = findByNumber(accountNumber);
        if (account.isPresent()) {
            account.get().setBalance(account.get().getBalance() + amount);
            accountDao.save(account.get());
        }
    }

    public void withdraw(String accountNumber, double amount) {
        Optional<Account> account = findByNumber(accountNumber);
        if (account.isPresent() && account.get().getBalance() >= amount) {
            account.get().setBalance(account.get().getBalance() - amount);
            accountDao.save(account.get());
        }
    }

    public void transfer(String fromAccountNumber, String toAccountNumber, double amount) {
        Optional<Account> fromAccount = findByNumber(fromAccountNumber);
        Optional<Account> toAccount = findByNumber(toAccountNumber);

        if (fromAccount.isPresent() && toAccount.isPresent()
                && fromAccount.get().getBalance() >= amount) {
            fromAccount.get().setBalance(fromAccount.get().getBalance() - amount);
            toAccount.get().setBalance(toAccount.get().getBalance() + amount);
            accountDao.save(fromAccount.get());
            accountDao.save(toAccount.get());
        }
    }
}
