package app.service;

import app.dao.Dao;
import app.models.Employer;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EmployerService {

    private final Dao<Employer> employerDao;

    public Employer getOneWithId(long employerId) {
        return employerDao.getOne(employerId);
    }

    public Employer create(Employer employer) {
        return employerDao.save(employer);
    }

    public Employer update(long employerId, String company, String address) {
        Employer existingEmployer = employerDao.getOne(employerId);
        if (existingEmployer != null) {
            existingEmployer.setCompany(company);
            existingEmployer.setAddress(address);
            return employerDao.save(existingEmployer);
        }
        return null;
    }

    public void deleteEmployer(long employerId) {
        Employer existingEmployer = employerDao.getOne(employerId);
        if (existingEmployer != null) {
            employerDao.delete(existingEmployer);
        }
    }
}
