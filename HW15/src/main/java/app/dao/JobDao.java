package app.dao;

import app.models.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public class JobDao extends GenericDao<Job> {
    public JobDao(JpaRepository<Job, Long> repository) {
        super(repository);
    }
}
