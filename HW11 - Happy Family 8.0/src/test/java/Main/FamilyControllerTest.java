package Main;

import Human.*;
import Pets.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class FamilyControllerTest {
    private Man Man1, Man2, FreeMan;
    private Woman Woman1, Woman2, FreeWoman;
    private DomesticCat Cat;
    private Pets.Dog Dog;
    private FamilyController familyController;
    private Family family1, family2;

    @BeforeEach
    public void setup() {
        Man1 = new Man("John", "Stevenson", 997555655000L);
        Woman1 = new Woman("Mary", "Johnson", 987555655000L);
        Man2 = new Man("David", "Newton", 807562055000L);
        Woman2 = new Woman("Lisa", "Watson", 804562055000L);
        FreeMan = new Man("Peter", "Bush", 966019655000L);
        FreeWoman = new Woman("Amanda", "Russel", 1078426055000L);
        Cat = new DomesticCat("Cat");
        Dog = new Dog("Pets.Dog");
        familyController = new FamilyController();
        family1 = familyController.createNewFamily(Woman1, Man1);
        family2 = familyController.createNewFamily(Woman2, Man2);
    }

    @Test
    void getAllFamiliesShouldGiveListOf2Families() {
        assertEquals(
                2,
                familyController.getAllFamilies().size()
        );
        assertEquals(
                family1,
                familyController.getAllFamilies().get(0)
        );
        assertEquals(
                family2,
                familyController.getAllFamilies().get(1)
        );
    }

    @Test
    void displayAllFamiliesShouldEqualExample() {
        String expectedOutput;
        expectedOutput = """
                1. Family{parents:{name='Mary', surname='Johnson', age=21 years, 11 months, 27 days}, {name='John', surname='Stevenson', age=21 years, 8 months, 3 days}}}
                2. Family{parents:{name='Lisa', surname='Watson', age=27 years, 9 months, 13 days}, {name='David', surname='Newton', age=27 years, 8 months, 10 days}}}
                """;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        familyController.displayAllFamilies();

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void getFamiliesBiggerThan1ShouldEqualExample() {
        String expectedOutput;
        expectedOutput = """
                Family{parents:{name='Mary', surname='Johnson', age=21 years, 11 months, 27 days}, {name='John', surname='Stevenson', age=21 years, 8 months, 3 days}}}
                Family{parents:{name='Lisa', surname='Watson', age=27 years, 9 months, 13 days}, {name='David', surname='Newton', age=27 years, 8 months, 10 days}}}
                """;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        familyController.getFamiliesBiggerThan(1);

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void getFamiliesLessThan3ShouldEqualExample() {
        String expectedOutput;
        expectedOutput = """
                Family{parents:{name='Mary', surname='Johnson', age=21 years, 11 months, 27 days}, {name='John', surname='Stevenson', age=21 years, 8 months, 3 days}}}
                Family{parents:{name='Lisa', surname='Watson', age=27 years, 9 months, 13 days}, {name='David', surname='Newton', age=27 years, 8 months, 10 days}}}
                """;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        familyController.getFamiliesLessThan(3);

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void countFamilies2MembersShouldEqual2() {
        assertEquals(
                2,
                familyController.countFamiliesWithMemberNumber(2)
        );
    }

    @Test
    void createNewFamilyShouldAddFamilyToLIst() {
        assertEquals(
                2,
                familyController.getAllFamilies().size()
        );
        assertDoesNotThrow(
                () -> familyController.createNewFamily(FreeWoman, FreeMan)
        );
        assertEquals(
                2 + 1,
                familyController.getAllFamilies().size()
        );
    }

    @Test
    void deleteFamilyByIndexShouldDeleteFamily() {
        assertEquals(
                2,
                familyController.getAllFamilies().size()
        );
        assertDoesNotThrow(
                () -> familyController.createNewFamily(FreeWoman, FreeMan)
        );
        assertEquals(
                2 + 1,
                familyController.getAllFamilies().size()
        );
        assertDoesNotThrow(
                () -> familyController.deleteFamilyByIndex(2)
        );
        assertEquals(
                2,
                familyController.getAllFamilies().size()
        );
    }

    @Test
    void bornChildShouldAdd1Child() {
        assertTrue(
                family1.getChildren().isEmpty()
        );
        assertTrue(
                family2.getChildren().isEmpty()
        );
        assertDoesNotThrow(
                () -> familyController.bornChild(family1, "Nash", "Rimma")
        );
        assertDoesNotThrow(
                () -> familyController.bornChild(family2, "Robert", "Anna")
        );
        assertEquals(
                1,
                family1.getChildren().size()
        );
        assertEquals(
                family1,
                familyController.getFamilyById(0)
        );
        assertEquals(
                1,
                family2.getChildren().size()
        );
        assertEquals(
                family2,
                familyController.getFamilyById(1)
        );
    }

    @Test
    void adoptChildShouldAddHumanToFamilyChildren() {
        assertTrue(
                family1.getChildren().isEmpty()
        );
        assertDoesNotThrow(
                () -> familyController.adoptChild(family1, FreeMan)
        );
        assertTrue(
                family1.getChildren().contains(FreeMan)
        );
    }

    @Test
    void deleteAllChildrenOlderThen18() {
        assertEquals(
                0,
                family1.getChildren().size()
        );
        assertDoesNotThrow(
                () -> familyController.adoptChild(family1, FreeMan)
        );
        assertDoesNotThrow(
                () -> familyController.bornChild(family1, "Nash", "Rimma")
        );
        assertEquals(
                2,
                family1.getChildren().size()
        );
        assertEquals(
                0,
                family2.getChildren().size()
        );
        assertDoesNotThrow(
                () -> familyController.adoptChild(family2, FreeWoman)
        );
        assertDoesNotThrow(
                () -> familyController.bornChild(family2, "Nick", "Santa")
        );
        assertEquals(
                2,
                family2.getChildren().size()
        );
        assertDoesNotThrow(
                () -> familyController.deleteAllChildrenOlderThen(18)
        );
        Screen.print(family2.getChildren().get(0).toString());
        assertEquals(
                1,
                family2.getChildren().size()
        );
        assertEquals(
                1,
                family2.getChildren().size()
        );
    }

    @Test
    void countSholdBe2() {
        assertEquals(
                2,
                familyController.count()
        );
    }

    @Test
    void getFamilyByIdShouldEqualFamily() {
        assertEquals(
                family1,
                familyController.getFamilyById(0)
        );
        assertEquals(
                family2,
                familyController.getFamilyById(1)
        );
    }

    @Test
    void getPetsShouldGetListPets() {
        family1.setPet(Cat);
        family1.setPet(Dog);
        assertEquals(
                2,
                familyController.getPets(0).size()
        );
        assertTrue(
                familyController.getPets(0).contains(Cat)
        );
        assertTrue(
                familyController.getPets(0).contains(Dog)
        );
    }

    @Test
    void addPet() {
        assertEquals(
                0,
                familyController.getPets(0).size()
        );
        assertDoesNotThrow(
                () -> familyController.addPet(0, Cat)
        );
        assertEquals(
                1,
                familyController.getPets(0).size()
        );
        assertTrue(
                familyController.getPets(0).contains(Cat)
        );
        assertEquals(
                0,
                familyController.getPets(1).size()
        );
        assertDoesNotThrow(
                () -> familyController.addPet(1, Dog)
        );
        assertEquals(
                1,
                familyController.getPets(1).size()
        );
        assertTrue(
                familyController.getPets(1).contains(Dog)
        );
    }
}