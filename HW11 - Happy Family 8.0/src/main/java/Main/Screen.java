package Main;

import java.util.Scanner;

public class Screen {

    static Scanner scan = new Scanner(System.in);

    public static String read() {
        return scan.nextLine();
    }

    public static void print(String line) {
        System.out.printf("%s\n", line);
    }

    public static void error(String line) {
        System.err.printf("%s\n", line);
    }
}