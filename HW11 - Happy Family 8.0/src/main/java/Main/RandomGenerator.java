package Main;


import Human.*;
import Schedule.*;

import java.util.*;

public class RandomGenerator {
    ArrayList<String> maleNames = new ArrayList<>(Arrays.asList("John", "Peter", "James", "Luke", "Daniel", "Mike", "Alex", "Tom", "Nick", "Rob"));
    ArrayList<String> femaleNames = new ArrayList<>(Arrays.asList("Amy", "Susie", "Kate", "Anna", "Lily", "Ella", "Britney", "Sophie", "Jenny", "Lucy"));
    ArrayList<String> surnames = new ArrayList<>(Arrays.asList("Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor"));
    Random random = new Random();

    public void generateTestFamilies(FamilyController familyController) {
        for (String surname : surnames) {
            Man father = generateMan(surname);
            Woman mother = generateWoman(surname);
            Family family = familyController.createNewFamily(mother, father);
            generateChildren(familyController, family);
        }
    }

    public Man generateMan(String surname) {
        return new Man(generateName(maleNames), surname, generateBirthDate(), generateIq(), generateSchedule());
    }

    public Woman generateWoman(String surname) {
        return new Woman(generateName(femaleNames), surname, generateBirthDate(), generateIq(), generateSchedule());
    }

    public String generateName(ArrayList<String> names) {
        return names.get(random.nextInt(names.size()));
    }

    public String generateSurname() {
        return surnames.get(random.nextInt(surnames.size()));
    }

    public long generateBirthDate() {
        return (random.nextInt(34) * 365 + random.nextInt(365)) * 86400000L;
    }

    public int generateIq() {
        return random.nextInt(170);
    }

    public Schedule generateSchedule() {
        Schedule schedule = new Schedule();
        while (random.nextInt(2) == 0) {
            schedule.addTask(
                    Arrays.asList(WeekDays.values())
                            .get(random.nextInt(WeekDays.values().length)),
                    Arrays.asList(Tasks.values())
                            .get(random.nextInt(Tasks.values().length))
            );
        }
        return schedule;
    }

    public void generateChildren(FamilyController familyController, Family family) {
        while (random.nextInt(2) == 0 && family.countFamily() < Main.MAX_SIZE) {
            if (random.nextInt(2) == 0)
                familyController.adoptChild(family, generateChild(family));
            else
                familyController.bornChild(family, generateName(maleNames), generateName(femaleNames));
        }
    }

    public Human generateChild(Family family) {
        long testBirthDate = family.getMother().getBirthDate() + 18 * 365 * 86400000L;
        Human newChild;
        newChild = (random.nextInt(2) == 0)
                ? new Man(generateName(maleNames), generateSurname(), testBirthDate, generateIq(), generateSchedule())
                : new Woman(generateName(femaleNames), generateSurname(), testBirthDate, generateIq(), generateSchedule());
        return newChild;
    }
}
