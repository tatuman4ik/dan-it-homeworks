package Main;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException() {
        super("This family already has max value of persons");
    }
}