package Main;

public class Main {
    public final static int MAX_SIZE = 10;
    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();
        StartMenu startMenu = new StartMenu(familyController);
    }
}
