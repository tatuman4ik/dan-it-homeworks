package Human;

import Main.Screen;
import Pets.Pet;
import Schedule.Schedule;

import java.time.Instant;

public final class Woman extends Human {

    public Woman(String name, String surname, long birthDate, int iq, Schedule schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Woman(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, int iq, Family family) {
        super(name, surname, Instant.now().toEpochMilli(), iq);
        setFamily(family);
    }

    public Woman(String name, String surname, long birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Woman() {
        super(null, null, Instant.now().toEpochMilli());
    }

    @Override
    public void greetPet() {
        if (getFamily() != null && !getFamily().getPets().isEmpty()) {
            for(Pet pet : getFamily().getPets()) {
                Screen.print("Привіт, " + pet.getNickname() + ", усі-пусі, йди до мене!");
                pet.respond(this);
            }
        }
    }

    public void makeUp() {
        Screen.print(getName() + " фарбується біля дзеркала");
    }
}
