package Human;

import Pets.*;
import org.jetbrains.annotations.NotNull;
import java.util.*;

public class Family implements HumanCreator {
    private final Human mother;
    private final Human father;
    private List<Human> children;
    private Set<Pet> pets;


    public Family() {
        throw new IllegalArgumentException("Мають бути двоє людей. Працює пустий конструктор");
    }

    public Family(@NotNull Woman mother, @NotNull Man father) {
        if (mother.getFamily() != null
                && father.getFamily() != null
                && mother.getFamily().equals(father.getFamily()))
            throw new IllegalArgumentException("Ці двоє вже й так в одній сім'ї");
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.children = new ArrayList<>();
        this.pets = new HashSet<>();
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human bornChild() {
        Random random = new Random();
        String surname = random.nextBoolean() ? mother.getSurname() : father.getSurname();
        int iq = (mother.getIq() + father.getIq()) / 2;
        List<String> males = new ArrayList<>(Arrays.asList("Володимир", "Любомир", "Святослав", "Григорій", "Євген"));
        List<String> females = new ArrayList<>(Arrays.asList("Марія", "Анна", "Ірина", "Ольга", "Катерина"));
        return random.nextBoolean() ? new Man(males.get(random.nextInt(males.size())), surname, iq, this)
                                    : new Woman(females.get(random.nextInt(females.size())), surname, iq, this);
    }

    public void addChild(@NotNull Human newChild) {
        if (!this.equals(newChild.getFamily())) newChild.setFamily(this);
        if (!children.contains(newChild)
                && !mother.equals(newChild)
                && !father.equals(newChild))
            children.add(newChild);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.size()) return false;
        Human childToDelete = children.get(index);
        children.remove(index);
        childToDelete.setFamily(null);
        return true;
    }

    public boolean deleteChild(@NotNull Human childToDelete) {
        if (children.contains(childToDelete)) {
            children.remove(childToDelete);
            childToDelete.setFamily(null);
            return true;
        }
        return false;
    }

    public List<Human> getChildren() {
        return children;
    }

    public int countFamily() { return children.size() + 2; }

    public void setPet(Pet pet) {
        setPet(pet, false);
    }

    public void setPet(Pet pet, boolean isFreePet) {
        if (pet != null) {
            pet.setFamily(this, isFreePet);
            pets.add(pet);
        }
    }

    public void deletePet(Pet pet) {
        pets.remove(pet);
    }

    public Set<Pet> getPets() {
        return pets;
    }

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder();
        message.append("Family{parents:")
                .append(getMother().getHumanFieldsInfo())
                .append(", ")
                .append(getFather().getHumanFieldsInfo())
                .append("}");
        if (!children.isEmpty()) {
            message.append(", children:");
            Iterator<Human> child = children.iterator();
            while (child.hasNext()) {
                message
                        .append("'")
                        .append(child.next()
                                .getHumanFieldsInfo())
                        .append("'");
                if (child.hasNext()) message.append(", ");
            }
        }
        if (!pets.isEmpty()) {
            message.append(", pets:");
            Iterator<Pet> pet = pets.iterator();
            while (pet.hasNext()) {
                message.append(pet.next().toString());
                if (pet.hasNext()) message.append(", ");
            }
        }
        message.append("}");
        return message.toString();
    }

    public String prettyFormat() {
        StringBuilder message = new StringBuilder("family:\n");
        message.append("\tparents:\n\t\tmother: ")
                .append(getMother().prettyFormat())
                .append("\n\t\tfather: ")
                .append(getFather().prettyFormat())
                .append("\n\tchildren: ");
        if (!children.isEmpty()) {
            for (Human human : children) {
                message.append("\n\t\t")
                        .append(human.getClass() == Man.class ? "boy: " : "girl: ")
                        .append(human.prettyFormat());
            }
        } else
            message.append("[null]");
        message.append("\n\tpets: ");
        if (!pets.isEmpty()) {
            for (Pet value : pets) {
                message.append("\n\t\t")
                        .append(value.prettyFormat());
            }
        } else {
            message.append("[null]");
        }
        return message.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        if (this == o) return true;
        Family family = (Family) o;
        if (hashCode() != family.hashCode()) return false;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Об'єкт Family буде видалений: " + this);
        } finally {
            super.finalize();
        }
    }
}