import java.util.Random;
import java.util.Scanner;
public class Numbers {
    public static void main(String[] args) {
        String name = enterName();
        int random = randomNumber(101);
        startGame(name, random);
    }
    public static void print(String line) {
        System.out.println(line);
    }
    public static String enterName() {
        print("Enter your name please");
        Scanner scan = new Scanner(System.in);
        return (scan.nextLine());
    }
    public static int randomNumber(int till) {
        Random rand = new Random();
        return rand.nextInt(till);
    }
    public static void startGame(String name, int random) {
        print("Let the game begin!");
        int number;
        do {
            number = enter();
        } while (!compare(random, number));
        print("Congratulations, " + name + "!");
    }
    public static int enter() {
        do {
            print("Enter your number from 0 to 100");
            Scanner scan = new Scanner(System.in);
            if(scan.hasNextInt()) {
                int number = scan.nextInt();
                if (number >= 0 && number <=100)
                    return (number);
            }
            print("Only number from 0 to 100 please");
        }while (true);
    }
    public static boolean compare(int random, int number) {
        if (random == number)
            return true;
        if (random < number) {
            print("Your number is too big. Please, try again.");
        } else
            print("Your number is too small. Please, try again.");
        return false;
    }
}
