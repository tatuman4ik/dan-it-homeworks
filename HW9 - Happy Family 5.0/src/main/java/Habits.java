public enum Habits {
    BARKING,
    MEOWING,
    SINGING,
    SWIMMING,
    JUMPING,
    HIDING,
    SLEEPING,
    EATING,
    DRINKING,
    RUNNING
}
