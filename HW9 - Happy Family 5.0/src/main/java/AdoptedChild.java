import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class AdoptedChild extends Human{
    public AdoptedChild(String firstName, String lastName, String birthDateStr, int iq) {
        super(firstName, lastName, LocalDate
                .parse(birthDateStr, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                .atStartOfDay()
                .toInstant(ZoneOffset.UTC)
                .toEpochMilli(),
                iq, new Schedule());
    }
}
