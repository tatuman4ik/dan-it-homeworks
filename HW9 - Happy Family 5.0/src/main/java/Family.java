import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Family implements HumanCreator {
    private final Human parent1;
    private final Human parent2;
    private List<Human> children;
    private Set<Pet> pets;


    public Family() {
        throw new IllegalArgumentException("Мають бути двоє людей. Працює пустий конструктор");
    }

    public Family(@NotNull Human parent1, @NotNull Human parent2) {
        if (parent1.equals(new Human())
                || parent2.equals(new Human())
                || parent1.equals(parent2))
            throw new IllegalArgumentException("Мають бути двоє людей");
        if (parent1.getFamily() != null
                && parent2.getFamily() != null
                && parent1.getFamily().equals(parent2.getFamily()))
            throw new IllegalArgumentException("Ці двоє вже й так в одній сім'ї");
        this.parent1 = parent1;
        this.parent2 = parent2;
        this.parent1.setFamily(this);
        this.parent2.setFamily(this);
        this.children = new ArrayList<>();
        this.pets = new HashSet<>();
    }

    public Human getParent1() {
        return parent1;
    }

    public Human getParent2() {
        return parent2;
    }

    public Human bornChild() {
        Random random = new Random();
        String surname = random.nextBoolean() ? parent1.getSurname() : parent2.getSurname();
        int iq = (parent1.getIq() + parent2.getIq()) / 2;
        List<String> males = new ArrayList<>(Arrays.asList("Володимир", "Любомир", "Святослав", "Григорій", "Євген"));
        List<String> females = new ArrayList<>(Arrays.asList("Марія", "Анна", "Ірина", "Ольга", "Катерина"));
        return random.nextBoolean() ? new Man(males.get(random.nextInt(males.size())), surname, iq, this)
                                    : new Woman(females.get(random.nextInt(females.size())), surname, iq, this);
    }

    public void addChild(@NotNull Human newChild) {
        if (!this.equals(newChild.getFamily())) newChild.setFamily(this);
        if (!children.contains(newChild)
                && !parent1.equals(newChild)
                && !parent2.equals(newChild))
            children.add(newChild);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.size()) return false;
        Human toDelete = children.get(index);
        children.remove(index);
        toDelete.setFamily(null);
        return true;
    }

    public boolean deleteChild(@NotNull Human child) {
        if (children.contains(child)) {
            children.remove(child);
            child.setFamily(null);
            return true;
        }
        return false;
    }

    public List<Human> getChildren() {
        return children;
    }

    public int countFamily() { return children.size() + 2; }

    public void setPet(Pet pet) {
        setPet(pet, false);
    }

    public void setPet(Pet pet, boolean isFreePet) {
        if (pet != null) {
            pet.setFamily(this, isFreePet);
            pets.add(pet);
        }
    }

    public void deletePet(Pet pet) {
        pets.remove(pet);
    }

    public Set<Pet> getPets() {
        return pets;
    }

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder(getClass().getSimpleName()
                + "{батьки:'" + getParent1().getHumanFieldsInfo()
                + "', '" + getParent2().getHumanFieldsInfo() + "'");
        if (!children.isEmpty()) {
            message.append(", children:");
            Iterator<Human> child = children.iterator();
            while (child.hasNext()) {
                message
                        .append("'")
                        .append(child.next()
                                .getHumanFieldsInfo())
                        .append("'");
                if (child.hasNext()) message.append(", ");
            }
        }
        if (!pets.isEmpty()) {
            message.append(", pets:");
            Iterator<Pet> pet = pets.iterator();
            while (pet.hasNext()) {
                message
                        .append("'")
                        .append(pet.next().toString())
                        .append("'");
                if (pet.hasNext()) message.append(", ");
            }
        }
        message.append("}");
        return message.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        if (this == o) return true;
        Family family = (Family) o;
        if (hashCode() != family.hashCode()) return false;
        return Objects.equals(parent1, family.parent1) &&
                Objects.equals(parent2, family.parent2);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(parent1, parent2, pets);
        result = 31 * result + children.hashCode();
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Об'єкт Family буде видалений: " + this);
        } finally {
            super.finalize();
        }
    }
}