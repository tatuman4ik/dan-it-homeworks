import java.util.Arrays;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
        Schedule scheduleForPetro = new Schedule();
        Screen.print("Створив новий пустий записник для Петра");
        Human Petro = new Human("Петро", "Коваленко", 229564800000L, 100, scheduleForPetro);
        Screen.print("Створив Петра з усіма полями. Ось він зараз:\n" + Petro);
        scheduleForPetro.addTask(WeekDays.MONDAY, Tasks.WORK);
        Screen.print("Створив запис через метод записника:\n" + Petro.getSchedule());
        Petro.setSchedule(WeekDays.SATURDAY, Tasks.PICNIC);
        Screen.print("Створив запис через метод самого Петра:\n" + scheduleForPetro);
        Screen.print("Ось Петро зараз:\n" + Petro);
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Human Marusya = new Human("Маруся", "Проценко", 261446400000L);
        Screen.print("Створив Марусю тільки з трьома полями. Ось вона зараз:\n" + Marusya);
        Marusya.setSchedule(WeekDays.TUESDAY, Tasks.LAUNDRY);
        Screen.print("Через метод Марусі додав запис у її записник");
        scheduleForPetro.addTask(WeekDays.TUESDAY, Tasks.GYM);
        Screen.print("Через метод записника Петра додав йому запис");
        Screen.print("Записник Марусі:\n" + Marusya.getSchedule());
        Screen.print("Записник Петра:\n" + scheduleForPetro);
        Schedule scheduleForMarusya = Marusya.getSchedule();
        Screen.print("Створив новий записник для Марусі та надав йому лінк на старий.");
        Marusya.setSchedule(WeekDays.THURSDAY, Tasks.CLEANING);
        Screen.print("Плани Марусі:\n" + scheduleForMarusya);
        Screen.error("Далі натисни ENTER");
        Screen.read();


        Family family1 = new Family(Petro, Marusya);
        Marusya.setSurname(Petro.getSurname());
        Screen.print("\nСтворив нову сім'ю з Петром та Марусею, Марусі дав прізвище Петра:\n" + family1);
        Pet Murchyk = new DomesticCat("Мурчик", 5, 57, new HashSet<>(Arrays.asList(Habits.MEOWING, Habits.HIDING)), false);
        Murchyk.setFamily(family1);
        Screen.print("Створив кота Мурчика і додав його в сім'ю Петра:");
        Petro.describePet();
        Petro.greetPet();
        Screen.print("Беру із методу Мурчика інформацію про його сім'ю:\n" + Murchyk.getFamily().toString());
        Screen.print("Петро намагається покормити Мурчика п'ять разів:");
        for (int i = 0; i < 5; i++) { Petro.feedPet(Murchyk, false); }
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Human Taras = new Human();
        Screen.print("\nСтворив Тараса по дефолту:\n" + Taras);
        Taras.setName("Тарас");
        Taras.setSurname("Коваленко");
        Taras.setBirthDate(967766400000L);
        Screen.print("Надав йому ім'я, прізвище та рік:\n" + Taras);
        Taras.setFamily(family1);
        Screen.print("Через метод Тараса додав його в сім'ю Коваленко. " +
                "Метод перевіряє батьків. Якщо вони є, то Тарас додається до списку дітей");
        Screen.print("Зараз сім'я Коваленко складається з:\n" + family1);
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Human Kateryna = new Human("Катерина", "Майстренко", 1072828800000L);
        Screen.print("\nСтворив Катерину тільки з трьома полями. Ось вона зараз:\n" + Kateryna);
        Family family2 = new Family(Kateryna, Taras);
        String surname = family2.getParent1().getSurname() + "-" + family2.getParent2().getSurname();
        Kateryna.setSurname(surname);
        Taras.setSurname(surname);
        Screen.print("Створив сім'ю Катерини і Тараса. Прізвище подвійне:\n" + family2);
        family2.setPet(Murchyk);
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Screen.print("\nТарас забрав Мурчика до себе:\n" + family2);
        Screen.print("Мурчик бачить свою сім'ю такою:\n" + Murchyk.getFamily().toString());
        Screen.print("А сім'я старших Коваленко тепер ось така:\n" + family1);
        Pet Dori = new Fish("Дорі");
        family1.setPet(Dori);
        Screen.print("\nДав старшим Коваленкам рибку Дорі, створивши її з двома параметрами");
        Petro.describePet();
        Petro.greetPet();
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Dori.setHabits(new HashSet<>(Arrays.asList(Habits.SWIMMING, Habits.SLEEPING)));
        Dori.setTrickLevel(80);
        Screen.print("\nВстановив для рибки звички та хитрість 80. Тепер вона така:\n" + Dori);
        Screen.print("Дивлюсь на сім'ю Дорі:\n" + Dori.getFamily().toString());
        Screen.print("Маруся намагається покормити Дорі п'ять разів:");
        for (int i = 0; i < 5; i++) { Marusya.feedPet(Dori, false); }
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Human HumanChildAdopted = new AdoptedChild("John", "Newton", "20/03/2016", 150);
        family2.addChild(HumanChildAdopted);
        Screen.print("\nСтворив усиновлену дитину і додав до family2\n" + HumanChildAdopted);
        Human HumanChild2 = family2.bornChild();
        Human HumanChild3 = family2.bornChild();
        Screen.print("Family2 born 2 людини");
        Screen.print(HumanChildAdopted.toString());
        Screen.print(HumanChild2.toString());
        Screen.print(HumanChild3.toString());
        Screen.print("Ось як виглядає family2:\n" +
                HumanChildAdopted.getFamily().toString());
        family2.deleteChild(1);
        Screen.print("Видалив дитину за індексом 1:\n" + family2);
        HumanChildAdopted.getFamily().deleteChild(HumanChildAdopted);
        Screen.print("Видалив дитину через її метод:\n" + family2);
        family1.setPet(Murchyk);
        family2.setPet(Dori);
        Screen.print("Віддав кота старшим Коваленкам, а рибку сім'ї Тараса:\n" + family1 + "\n" + family2);
        Screen.print("Нова дитина в сім'ї:" + family2.bornChild());
        Screen.error("Кінець тесту.");
        for (int i = 0; i < 10000; i++) { // в мене вже починає видаляти непотрібні об'єкти
            Human human = new Human(String.valueOf(i), null, 0);
        }
    }
}
