import java.util.Objects;
import java.util.Random;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;
    private boolean hungry;
    private boolean innocent;
    private Family family;

    public Pet(String species, String nickname, int age, int trickLevel,
               String[] habits, boolean hungry, boolean innocent) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.hungry = hungry;
        this.innocent = innocent;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this(species, nickname, age, trickLevel, habits, false, false);
    }

    public Pet(String species, String nickname) {
        this(species, nickname, 0, 0, new String[]{}, false, false);
    }

    public Pet() {};

    public void eat() {
        Screen.print("Я - їм!");
        setHungry(false);
    }

    public void respond(Human human) {
        Screen.print("Привіт, хазяїн " + human.getName() + "! Я " + getNickname() + ". Я скучив!");
    }

    public void foul() {
        Screen.print("Треба добре замісти сліди...");
        int level = getTrickLevel();
        if (isLucky()) {
            setInnocent(true);
            if (level < 100) level++;
        } else {
            setInnocent(false);
            if (level > 0) level--;
        }
        setTrickLevel(level);
    }

    public boolean isLucky() {
        Random random = new Random();
        if (getTrickLevel() > random.nextInt(101))
            return true;
        return false;
    }

    @Override
    public String toString() {
        String message = "";
        if (getSpecies() != null) message += getSpecies() + "{";
        if (getNickname() != null) message += "nickname='" + getNickname() + "'";
        message += ", age=" + getAge() + ", trickLevel=" + getTrickLevel();
        if (getHabits() != null) message += ", habits=[" + String.join(", ", getHabits()) + "]}";
        return message;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getSpecies() {
        return species;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHungry(boolean hungry) {
        this.hungry = hungry;
    }

    public boolean isHungry() {
        return hungry;
    }

    public void setInnocent(boolean innocent) {
        this.innocent = innocent;
    }

    public boolean isInnocent() {
        return innocent;
    }

    public void setFamily(Family family) {
        setFamily(family, false);
    }

    public void setFamily(Family family, boolean isFreePet) {
        if (family != null) {
            if (!isFreePet) {
                if (this.hasFamily()) this.family.deletePet();
                this.family = null;
                family.setPet(this, true);
            }
        }
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public boolean hasFamily() {
        return this.family != null;
    }

    public void deleteFamily() {
        this.family = null;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        if (this == o) return true;
        Pet pet = (Pet) o;
        if (hashCode() != pet.hashCode()) return false;
        return age == pet.age &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname) &&
                Objects.equals(trickLevel, pet.trickLevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }
}
