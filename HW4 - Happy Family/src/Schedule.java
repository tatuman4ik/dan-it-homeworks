public class Schedule {
    private final int days = Week.DAYS_IN_WEEK;
    private String[][] schedule = new String[days][2];
    private TaskList tasks = new TaskList();
    private Week week = new Week();

    public Schedule() {
        for (int i = 0; i < days; i++) {
            this.schedule[i][0] = this.week.getDayName(i);
            this.schedule[i][1] = this.tasks.getTask(i);
        }
    }

    public void setOneTask(int dayNum) {
        this.schedule[dayNum][1] = this.tasks.setTask(dayNum);
    }

    public void setOneTask(int dayNum, String task) {
        this.schedule[dayNum][1] = this.tasks.setTask(dayNum, task);
    }

    public String getDayName(int dayNum) { return this.week.getDayName(dayNum); }

    public String getTask(int dayNum) {
        return this.schedule[dayNum][1];
    }

    public int getDays() { return days; }
}