public class Week {
    final static int DAYS_IN_WEEK = 7;
    private String[] daysOfWeek = new String[DAYS_IN_WEEK];

    public Week() {
        this.daysOfWeek[0] = "Понеділок";
        this.daysOfWeek[1] = "Вівторок";
        this.daysOfWeek[2] = "Середа";
        this.daysOfWeek[3] = "Четверг";
        this.daysOfWeek[4] = "Пʼятниця";
        this.daysOfWeek[5] = "Субота";
        this.daysOfWeek[6] = "Неділя";
    }

    public String getDayName(int dayNum) {
        return this.daysOfWeek[dayNum];
    }
}