public class TaskList {
    private final int daysInWeek = Week.DAYS_IN_WEEK;
    private String[] tasks = new String[daysInWeek];

    public TaskList() { for (String task: tasks) { task = ""; }}

    public String getTask(int taskNum) {
        return this.tasks[taskNum];
    }

    public String setTask(int dayNum) {
        Screen.print("Уведіть свої нотатки");
        this.tasks[dayNum] = Screen.read();
        return this.tasks[dayNum];
    }

    public String setTask(int dayNum, String task) {
        this.tasks[dayNum] = task;
        return this.tasks[dayNum];
    }
}