import java.time.Year;

public class Main {

    public static void main(String[] args) {
        Schedule scheduleForPetro = new Schedule();
        Screen.print("Створив новий пустий записник для Петра");
        Human Petro = new Human("Петро", "Коваленко", Year.of(1970), 100, scheduleForPetro);
        Screen.print("Створив Петра з усіма полями. Ось він зараз:\n" + Petro);
        scheduleForPetro.setOneTask(0, "Робота");
        Screen.print("Створив запис через метод записника:\n" + Petro.dailyTasks());
        Petro.setSchedule(5, "Пікнік");
        Screen.print("Створив запис через метод самого Петра:\n" + Petro.dailyTasks());
        Screen.print("Ось Петро зараз:\n" + Petro);
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Human Marusya = new Human("Маруся", "Проценко", Year.of(1970));
        Screen.print("Створив Марусю тільки з трьома полями. Ось вона зараз:\n" + Marusya);
        Marusya.setSchedule(1, "Прання");
        Screen.print("Через метод Марусі додав запис у її записник");
        scheduleForPetro.setOneTask(1, "Футбол");
        Screen.print("Через метод записника Петра додав йому запис");
        Screen.print("Записник Марусі:\n" + Marusya.dailyTasks());
        Screen.print("Записник Петра:\n" + Petro.dailyTasks());
        Schedule scheduleForMarusya = Marusya.getSchedule();
        Marusya.setSchedule(scheduleForMarusya);
        Screen.print("Створив новий пустий записник для Марусі та скопіював старий до нього.");
        Screen.error("Можна додати їй плани на четверг вручну:");
        Marusya.setSchedule(3);
        Screen.print("Плани Марусі:\n" + Marusya.dailyTasks());
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Family family1 = new Family(Petro, Marusya);
        Marusya.setSurname(Petro.getSurname());
        Screen.print("\nСтворив нову сім'ю з Петром та Марусею, Марусі дав прізвище Петра:\n" + family1);
        Pet Murchyk = new Pet("Кіт", "Мурчик", 5, 57, new String[]{"спати", "гуляти", "гратися"});
        Murchyk.setFamily(family1);
        Screen.print("Створив кота Мурчика і додав його в сім'ю Петра:");
        Petro.describePet();
        Petro.greetPet();
        Screen.print("Беру із методу Мурчика інформацію про його сім'ю:\n" + Murchyk.getFamily().toString());
        Screen.print("Петро намагається покормити Мурчика п'ять разів:");
        for (int i = 0; i < 5; i++) { Petro.feedPet(false); }
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Human Taras = new Human();
        Screen.print("\nСтворив Тараса по дефолту:\n" + Taras);
        Taras.setName("Тарас");
        Taras.setSurname("Коваленко");
        Taras.setYear(Year.of(1991));
        Screen.print("Надав йому ім'я, прізвище та рік:\n" + Taras);
        Taras.setFamily(family1);
        Screen.print("Через метод Тараса додав його в сім'ю Коваленко. " +
                "Метод перевіряє батьків. Якщо вони є, то Тарас додається до списку дітей");
        Screen.print("Зараз сім'я Коваленко складається з:\n" + family1);
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Human Kateryna = new Human("Катерина", "Майстренко", Year.of(1989));
        Screen.print("\nСтворив Катерину тільки з трьома полями. Ось вона зараз:\n" + Kateryna);
        Family family2 = new Family(Kateryna, Taras);
        String surname = family2.getParent1().getSurname() + "-" + family2.getParent2().getSurname();
        Kateryna.setSurname(surname);
        Taras.setSurname(surname);
        Screen.print("Створив сім'ю Катерини і Тараса. Прізвище подвійне:\n" + family2);
        family2.setPet(Murchyk);
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Screen.print("\nТарас забрав Мурчика до себе:\n" + family2);
        Screen.print("Мурчик бачить свою сім'ю такою:\n" + Murchyk.getFamily().toString());
        Screen.print("А сім'я старших Коваленко тепер ось така:\n" + family1);
        family1.setPet(new Pet("Рибка", "Дорі"));
        Screen.print("\nДав старшим Коваленкам рибку Дорі, передавши два її параметри");
        Petro.describePet();
        Petro.greetPet();
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Petro.getFamily().getPet().setHabits(new String[]{"плавати", "здійснювати бажання"});
        family1.getPet().setTrickLevel(80);
        Screen.print("\nВстановив для рибки звички (через Петра) та хитрість 80 (через сім'ю). Тепер вона така:\n" + family1.getPet().toString());
        Pet Dori = family1.getPet();
        Dori.setFamily(family1);
        Screen.print("Створив нову тварину і переназначив лінк на Дорі. Дивлюсь на сім'ю \"нової\" Дорі:\n" + Dori.getFamily().toString());
        Screen.print("Маруся намагається покормити Дорі п'ять разів:");
        for (int i = 0; i < 5; i++) { Marusya.feedPet(false); }
        Screen.error("Далі натисни ENTER");
        Screen.read();

        Human Lyudmyla = new Human();
        Lyudmyla.setName("Людмила");
        Human Oleksiy = new Human();
        Oleksiy.setName("Олексій");
        Human Dmytro = new Human();
        Dmytro.setName("Дмитро");
        Screen.print("\nСтворив 3 людини по дефолту, потім надав їм ім'я");
        Screen.print(Lyudmyla.toString());
        Screen.print(Oleksiy.toString());
        Screen.print(Dmytro.toString());
        family2.setChildren(new Human[]{Lyudmyla, Oleksiy, Dmytro});
        for (Human human: family2.getChildren()) { human.setSurname(surname); }
        Screen.print("Додав їх одним масивом до сім'ї Тараса, додав прізвища. Ось як це бачить Людмила:\n" +
                Lyudmyla.getFamily().toString());
        family2.deleteChild(1);
        Screen.print("Видалив дитину за індексом 1:\n" + family2);
        Lyudmyla.getFamily().deleteChild(Lyudmyla);
        Screen.print("Видалив Людмилу через її метод:\n" + family2);
        family1.setPet(Murchyk);
        family2.setPet(Dori);
        Screen.print("Віддав кота старшим Коваленкам, а рибку сім'ї Тараса:\n" + family1 + "\n" + family2);
        Screen.error("Кінець тесту.");
    }
}
